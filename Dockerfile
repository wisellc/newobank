# Base on offical Node.js Alpine image
FROM node:14.4.0

# Set working directory
WORKDIR /usr/app

# Copy package.json and package-lock.json before other files
# Utilise Docker cache to save re-installing dependencies if unchanged
COPY ./package*.json ./

# Copy patch files
COPY ./patches ./patches

# Install dependencies
RUN npm install

# Globally node-sass from NPM
RUN npm rebuild node-sass --force

# Copy all files
COPY ./ ./

# Run patches
RUN npm run postinstall

# Build app
RUN npm run build

# Expose the listening port
EXPOSE 3000

# Run container as non-root (unprivileged) user
# The node user is provided in the Node.js Alpine base image
USER node

# Run npm start script with PM2 when container starts
CMD [ "npm", "start" ]