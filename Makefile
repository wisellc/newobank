BRANCH = master
DEV-BRANCH = develop

deploy:
	git checkout -f master
	git pull
	git checkout -f ${BRANCH}
	cp ./.env.prod ./.env
	docker-compose up -d --build

dev-deploy:
	git checkout -f develop
	git pull
	git checkout -f ${DEV-BRANCH}
	cp ./.env.dev ./.env
	docker-compose -f docker-compose.dev.yml up -d --build

cc:
	docker exec -it ssr_obank_nextjs_1 rm -rf /tmp/hdc
	docker restart ssr_obank_nextjs_1
	docker-compose up -d --build
