##How to install locally

1. `git clone git@bitbucket.org:wisellc/newobank.git`
2.  `cp .env.dev .env`
3. `npm install`
4. `npm run dev`

#### Important!

Before commit run `npm run eslint` to check project problems

Test deploy