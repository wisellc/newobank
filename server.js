const cacheableResponse = require('cacheable-response')
const express = require('express')
const next = require('next')
const path = require('path');
const compression = require('compression')
const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const Keyv = require('keyv');
const serverProxy = require('./config/Server/serverProxy');
const handle = app.getRequestHandler();
const addIsBot = require('./config/Server/addIsBot');
const cacheStore = new Keyv({namespace: 'ssr-cache'});
const ttl = 1000 * 60 * 60; // 1hour

const cacheManager = cacheableResponse({
    ttl,
    get: async ({ req, res }) => {
        const data = await app.renderToHTML(req, res, req.path, {
            ...req.query,
            ...req.params,
        })
        if (res.statusCode === 404) {
            res.end(data)
            return
        }

        return { data }
    },

    send: ({data, res}) => {
        res.send(data);
    },
    getKey: ({ req }) => {
        return req.url + (req.isBot ? '_bot' : '')
    },
    cache: cacheStore,
    compress: true
});

function clearCompleteCache(res, req) {
    cacheStore.clear();
    res.status(200);
    res.send({
        path: req.hostname + req.baseUrl,
        purged: true,
        clearedCompleteCache: true
    });
    res.end();
}

app.prepare().then(() => {
    const server = express()
    server.use(express.static(path.join(__dirname, 'public')));
    server.use('/_next', express.static(path.join(__dirname, '.next')));
    server.get('/_next/*', handle);
    server.use(addIsBot);
    server.use(compression())
    server.use(serverProxy)

    server.get('*', (req, res) => {
        if (dev) {
            res.setHeader('X-Cache-Status', 'DISABLED');
            handle(req, res)
        } else if (req.url === process.env.CLEAR_CACHE_URL) {
            clearCompleteCache(res, req)
        } else {
            cacheManager({req, res})
        }
    })

    server.all('*', (req, res) => {
        handle(req, res)
    })

    server.listen(port, (err) => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
})
