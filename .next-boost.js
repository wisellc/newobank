const isbot = require('isbot');

module.exports = {
    userAgentFilter: (userAgent) => {
        return !isbot(userAgent)
    },
    paramFilter: (p) => p !== 'onlysms'
}
