const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const withPWA = require('next-pwa');
const webpack = require('webpack')
const path = require('path')

const prod = process.env.NODE_ENV === 'production';
module.exports = withPWA(
    withCSS(
        withSass(
            {
                webpack(config) {
                    config.resolve.extensions.push('.scss');
                    config.optimization.minimizer = [];
                    config.optimization.minimizer.push(
                        new OptimizeCSSAssetsPlugin({
                            cssProcessorOptions: {
                                map: {
                                    inline: false
                                }
                            }
                        })
                    );
                    if (prod) {

                        config.optimization.minimizer.push(new TerserPlugin({
                            cache: false
                        }))
                        config.optimization.minimize = true;
                    }
                    config.plugins.push(
                        new webpack.ProvidePlugin({
                            __: path.join(__dirname, 'config', 'TranslationFunction'),
                            getLang: path.join(__dirname, 'config', 'SiteLang'),
                            React: 'react'
                        })
                    );
                    config.module.rules.push({
                        test: /\.(scss)$/,
                        use: [
                            {
                                loader: 'sass-resources-loader',
                                options: {
                                    resources: path.join(__dirname, 'src', 'styles', 'helpers', '_mixins.scss')
                                }
                            }
                        ]
                    });
                    config.module.rules.push({
                        test: /\.(png|svg|jpg|gif|eot|webp|jpeg|ttf|woff|woff2)$/,
                        use: {
                            loader: 'url-loader',
                            options: {
                                limit: 100000
                            }
                        }
                    })
                    return config
                },
                env: {
                    HTTP_BASE_AUTH: process.env.HTTP_BASE_AUTH,
                    REACT_APP_API_URL: process.env.REACT_APP_API_URL
                },
                compress: prod,
                sassLoaderOptions: {
                    sourceMap: !prod
                },
                cssLoaderOptions: {
                    sourceMap: !prod,
                    importLoaders: 2,
                    localIdentName: '[local]___[hash:base64:5]',
                },
                async rewrites() {
                    return [
                        ...(prod ? [] : [
                            {
                                source: '/sites/default/:path*',
                                destination: `${process.env.REACT_APP_API_URL}/sites/default/:path*`
                            },
                        ])
                    ]
                },
                pwa: {
                    dest: 'public',
                    disable: !prod,
                    sw: 'service-worker.js',
                }
            })));
