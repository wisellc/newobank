const authority = process.env.REACT_APP_API_URL;
const fetch = require('node-fetch');
const dev = process.env.NODE_ENV !== 'production'
global.Headers = global.Headers || require('fetch-headers');
const headers = new Headers();
let enableImages = false;
if(process.env.HTTP_BASE_AUTH){
    headers.set('Authorization', process.env.HTTP_BASE_AUTH);
    enableImages = true
}
module.exports = async (req, res, next) => {
    const apiFiles = [
        '/core/misc/',
        '/sitemap.xml',
        'sitemap.xsl'
    ];

    if (dev || enableImages) {
        apiFiles.push('/sites/default/')
    }

    if (apiFiles.some((a) => req.path.indexOf(a) !== -1)) {
        const requestOptions = {
            method: 'get',
            headers,
        };
        const copyHeaders = ['Content-Type', 'cache-control', 'expires', 'etag', 'last-modified'];
        const r = await fetch(authority + req.path, requestOptions)
            .then((a) => {
                copyHeaders.forEach((b) => res.setHeader(b, a.headers.get(b)));
                return a.arrayBuffer();
            });

        return res.send(Buffer.from(r));
    } else if (next) {
        next();
    }
};
