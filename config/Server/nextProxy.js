global.Headers = global.Headers || require('fetch-headers');
const headers = new Headers();
// if (process.env.HTTP_BASE_AUTH) {
//     headers.set('Authorization', process.env.HTTP_BASE_AUTH);
// }

export const isProxyUrl = (asPath) => {
    const apiFiles = [
        '/core/misc/',
        '/sites/default/',
        '/sitemap.xml',
        'sitemap.xsl'
    ];
    return apiFiles.some((a) => asPath.indexOf(a) !== -1)
}

export const proxy = async (asPath, res) => {
    const requestOptions = {
        method: 'get',
        headers,
    };
    const copyHeaders = ['Content-Type', 'cache-control', 'expires', 'etag', 'last-modified'];
    const r = await fetch(process.env.REACT_APP_API_URL + asPath, requestOptions)
        .then((a) => {
            copyHeaders.forEach((b) => res.setHeader(b, a.headers.get(b)));
            return a.arrayBuffer();
        });
    res.write(Buffer.from(r))
    res.end()
}
