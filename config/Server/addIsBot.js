const isbot = require('isbot');

module.exports = (req, res, next) => {
    const isBot = isbot(req.headers['user-agent']);
    if (isBot) {
        // eslint-disable-next-line no-param-reassign
        req.isBot = true;
    }
    next();
};
