const ru = require('../../i18n/ru_RU');
const uk = require('../../i18n/ua_UK');

const mockTranslations = (format, ...args) => {
    let i = 0;
    if (getLang() === 'ru') {
        return (ru[format] || format || '').replace(/%s/g, () => args[i++])
    } else if (getLang() === 'uk') {
        return (uk[format] || format || '').replace(/%s/g, () => args[i++])
    }
    return (format || '').replace(/%s/g, () => args[i++]);
};

module.exports = mockTranslations;
