const LangDetector = {
    lang: false,
    set: function (asPath) {
        if (asPath.indexOf('/uk') !== -1) {
            this.lang = 'uk'
        } else if (asPath.indexOf('/ru') !== -1) {
            this.lang = 'ru'
        }
    }

}

export default LangDetector;
