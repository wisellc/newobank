import LangDetector from './LangDetector';

const getSiteLang = (url) => {
    if(url){
        LangDetector.set(url);
    }
    return LangDetector.lang || 'uk'
}

module.exports = getSiteLang;
