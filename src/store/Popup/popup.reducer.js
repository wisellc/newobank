import {getServerStore, isClient} from 'Util/ServerRedux';

export const ACTIVATE_POPUP = 'ACTIVATE_POPUP';
export const DISABLE_POPUP = 'DISABLE_POPUP';

const popupInit = {
    data: {},
    popup: false
}
const initialState = () => isClient()
    ? getServerStore().popupReducer || popupInit
    : popupInit

export function popupReducer(state = initialState(), action) {
    const {type, data, popup} = action;
    switch (type) {
        case ACTIVATE_POPUP:
            return {...state, data, popup};
        case DISABLE_POPUP:
            return {...state, data: false, popup: false}
        default:
            return state
    }
}
