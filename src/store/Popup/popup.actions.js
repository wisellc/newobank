import {ACTIVATE_POPUP, DISABLE_POPUP} from 'Store/Popup/popup.reducer';

export const activatePopup = (popup, data) => ({
    type: ACTIVATE_POPUP,
    popup,
    data
});

export const disablePopup = () => ({
    type: DISABLE_POPUP
})
