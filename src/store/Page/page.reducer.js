import {getServerStore, isClient} from 'Util/ServerRedux';

export const GET_PAGE = 'GET_PAGE'

const pageInit = {
    page: {
        node: {
            attributes: false
        },
        header: {

        }
    }
}
const initialState = () => isClient()
    ? getServerStore().pageReducer || pageInit
    : pageInit

export function pageReducer(state = initialState(), action) {
    const {type, page} = action;
    switch (type) {
        case GET_PAGE:
            return {...state, page};
        default:
            return state
    }
}
