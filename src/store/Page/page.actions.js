import {GET_PAGE} from 'Store/Page/page.reducer';

export const updatePage = (page) => ({
    type: GET_PAGE,
    page
});
