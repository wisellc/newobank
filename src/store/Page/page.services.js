import {sendGetByUrl} from 'Util/Request';

export function getFullPageSSR(alias) {
    const pathname = alias.replace('/' + getLang(alias), '').replace('?', '&').split('#')[0];
    const url = process.env.REACT_APP_API_URL + '/' + getLang(alias) + '/wise/reactapi/requestssr?url=' +
        ((pathname === '/' || !pathname) ? 'home' : pathname);
    return sendGetByUrl(url, true);
}
