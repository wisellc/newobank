import {
    combineReducers,
    createStore
} from 'redux';
import {pageReducer} from 'Store/Page/page.reducer';
import {getServerStore, isClient} from 'Util/ServerRedux';
import {popupReducer} from 'Store/Popup/popup.reducer';

export const getStaticReducers = () => ({
    pageReducer,
    popupReducer
})

export function createReducer(asyncReducers) {
    return combineReducers({
        ...getStaticReducers(),
        ...asyncReducers
    });
}

export const store = createStore(
    createReducer()
);

/**
 * Configure the store
 * @namespace Store/Index/configureStore
 * */
export default function configureStore() {
    // Add a dictionary to keep track of the registered async reducers
    store.asyncReducers = {};

    if (isClient()) {
        store.asyncReducers = getServerStore();
    }

    // Create an inject reducer function
    // This function adds the async reducer, and creates a new combined reducer
    store.injectReducer = (key, asyncReducer) => {
        store.asyncReducers[key] = asyncReducer;
        store.replaceReducer(createReducer(store.asyncReducers));
    };

    // Return the modified store
    return store;
}

export function getStore() {
    return store;
}
