export const getUrlPramsString = (asPath) => {
    return asPath.split('?')[1] ? '?' + asPath.split('?')[1] : '';
}
