import BlockSorting, {disabledTitle} from 'Util/BlockSorting';
import {GetBodyValuePropTypes, WithBlockTitlePropTypes} from 'Types/index';
import {getParagraphType} from 'Paragraph/index';
import Html from 'Components/Html';
import Views from 'Views/views';
import LBProvider from 'Util/LayoutBuilder/LB.provider';

export function layoutBuilderComponentsSort(data) {
    const blocks = [];
    if (!data) {
        console.log('Layout builder from viewMode');
        return false;
    }
    data.forEach(item => {
        const components = Object.values(item.components)
            .sort((a, b) => parseInt(a.weight) < parseInt(b.weight) ? -1 : 1)

        const layout = {
            components,
            layout_id: item.layout_id + (item.layout_settings.column_widths ? ' ' + item.layout_id + '-' + item.layout_settings.column_widths : ''),
        };
        blocks.push(layout);
    });
    blocks.forEach((item, index) => {
        const regions = [];
        item.components.forEach(comp => {
            if (typeof regions[comp.region] === 'undefined') {
                regions[comp.region] = []
            }
            regions[comp.region].push(comp);
        });
        blocks[index].regions = regions;
    });
    return blocks;
}

export function WithBlockTitle(props) {
    const {
        children,
        config: {id, className, field_block_title, type}
    } = props

    return <article className={className} key={id} id={id}>
        {(field_block_title && !disabledTitle.includes(type)) &&
            <h3 className={'section-title'}>
                <Html content={field_block_title}/>
            </h3>
        }
        {children}
    </article>
}

WithBlockTitle.propTypes = WithBlockTitlePropTypes

export const getBlockSettings = (block) => {
    const {configuration: {provider}, additional: {css_classes = ''}, data, included} = block;
    if (!data) {
        console.log(block);
        return null
    }
    const {type, id, attributes} = data;

    if (!attributes.status) {
        console.warn(id + ' disabled!')
        return null;
    }

    const block_type = getParagraphType(type);
    const className = css_classes + ' block ' + block_type;
    const config = {
        type: block_type,
        id,
        className,
        content_type: provider,
        field_block_title: attributes.field_block_title
    };

    return (
        <WithBlockTitle config={config}>
            <BlockSorting
                config={config}
                data={data}
                included={included}
            />
        </WithBlockTitle>
    )
}

export const GetBodyValue = ({value, className = ''}) => {
    if (!(value?.processed)) {
        return ''
    }
    let __html = value.processed;

    return __html && <div
        className={'full_html ' + className}
    >
        <Html content={__html}/>
    </div>
}


GetBodyValue.propTypes = GetBodyValuePropTypes

export const getContentSettings = (block) => {
    if(!block.configuration?.provider){
        console.log(block)
        return null;
    }
    if (block.configuration.provider === 'layout_builder') {
        return <LBProvider block={block}/>;
    } else if (block.configuration.provider === 'block_content') {
        return getBlockSettings(block);
    } else if(block.configuration.provider === 'views') {
        return <Views {...block}/>
    }
}

export function regionsRender({regions, layout_id}, index) {
    const keys = Object.keys(regions);
    return (
        <section className={layout_id} key={index}>{keys.sort().map((reg, regIndex) =>
            <div className={'region_' + reg} key={regIndex}>{regions[reg].map((block, index2) =>
                <React.Fragment key={index + reg + index2}>
                    {getContentSettings(block)}
                </React.Fragment>)}
            </div>)}
        </section>
    )
}

export function getLayoutBuilderComponents(attributes) {
    if (attributes.layout_builder__layout) {
        return layoutBuilderComponentsSort(attributes.layout_builder__layout)
    }

    if (attributes.view_mode.third_party_settings.layout_builder.sections) {
        return layoutBuilderComponentsSort(
            attributes.view_mode.third_party_settings.layout_builder.sections
        )
    }
    return []
}

export function renderLayoutBuilder(node) {
    const {attributes} = node;
    return getLayoutBuilderComponents(attributes)
        .map(regionsRender)

}
