import {GetBodyValue} from 'Util/LayoutBuilder/index';
import {useSelector} from 'react-redux';
import { shape, string} from 'prop-types';
import Image from 'next/image';
import 'Styles/components/_expertLB.sass'


const LBProvider = ({block}) => {
    const {configuration: {id}} = block;
    const {node: {attributes, relationships}} = useSelector(state => state.pageReducer.page)
    const idArr = id.split(':');
    const field = idArr[idArr.length - 1];

    const attrField = attributes[field];
    const relatField = relationships[field];

    if (attrField) {
        const attrMap = {
            body: () => <GetBodyValue className="layout_onecol__body" value={attrField}/>,
            title: () => <h1 className="layout_onecol__title">{attrField}</h1>,
            field_data: () => (
                <footer className="layout_onecol__field_data">
                    <time className={'date'}>{attrField}</time>
                </footer>
            )
        }

        if (attrMap[field]) {
            return attrMap[field]();
        }
        console.log(field + ' render as default attribute Field!');
        return attrField.toString();
    }

    if (relatField) {
        const relatMap = {
            field_blogtags: () => (
                <ul className="expert_article__tags">
                    {relatField.data.map(({attributes: {name}}) => <li key={name}>{name}</li>)}
                </ul>
            ),
            field_blog_image: () => {
                const {data} = relatField;
                if (!data) {
                    return null;
                }
                const {attributes, meta} = data;
                return (
                    <div className="expert_article__img">
                        <Image src={attributes.uri.url} {...meta}/>
                    </div>
                )
            }
        }
        if (relatMap[field]) {
            return relatMap[field]();
        }
        console.log(field + ' render as default relationships Field!');
        return relatField.toString();
    }
    return null;
}


LBProvider.propTypes = {
    block: shape({
        configuration: shape({
            id: string
        })
    })
}

export default LBProvider;
