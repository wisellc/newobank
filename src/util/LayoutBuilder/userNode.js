import {useState} from 'react';
import 'Styles/components/_userNode.sass';
import SocialNetworks from 'Components/SocialNetworks/SocialNetWorks';
import {arrayOf, shape, string} from 'prop-types';
import {GetBodyValue} from 'Util/LayoutBuilder/index';
import Image from 'next/image';


const UserNode = ({node}) => {
    const [isOpen, setIsOpen] = useState(false);

    const {
        attributes,
        relationships: {
            user_picture: {data}
        }
    } = node;

    return (
        <div className="expert-info">
            <div className="container">
                <div className="expert-info__inner">
                    <div className='expert-info__img'><Image src={data.attributes.uri.url} {...data.meta}/></div>
                    <div className="expert-info__wrap">
                        <span>{attributes.field_user_name + ' ' + attributes.field_user_surname}</span>
                        <p className="expert-info__subtitle">{attributes.field_dosvid_roboty}</p>

                        <SocialNetworks field_sotsialni_merezhi={attributes.field_sotsialni_merezhi}/>

                        <button className={isOpen ? 'active' : ''}
                                onClick={() => setIsOpen(!isOpen)}>

                            {__('About the author')}
                        </button>

                        <GetBodyValue value={attributes.field_pro_avtora}
                                      className={'expert-info__tab' + (isOpen ? ' active' : '')}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

UserNode.propTypes = {
    node: shape({
        attributes: shape({
            display_name: string,
            field_user_name: string,
            field_user_surname: string,
            field_dosvid_roboty: string,
            field_sotsialni_merezhi: arrayOf(shape({
                title: string,
                uri: string
            })),
            field_pro_avtora: shape({processed: string})
        }),
    })
}

export default UserNode
