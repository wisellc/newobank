export const toggleScroll = (state) => document.documentElement.classList.toggle('scrollDisabled', !state);

export const isScrollDisabled = () => document.documentElement.classList.contains('scrollDisabled');

export const getScrollbarWidth = () => {
    const scrollDiv = document.createElement('div');
    scrollDiv.className = 'scrollbar-measure';
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.style.paddingRight = `${scrollbarWidth}px`;
    document.body.removeChild(scrollDiv);
};