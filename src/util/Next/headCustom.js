/*
  NOTE: This modifies next.js internal api behavior and may break your application.
  Tested on next.js version: 10.0.1
*/

import { Head } from 'next/document';

class HeadCustom extends Head {
    getCssLinks(docFiles) {
        const files = [...docFiles.pageFiles, ...docFiles.sharedFiles, ...docFiles.sharedFiles];
        const { assetPrefix } = this.context;
        const cssFiles = files && files.length ? files.filter((f) => /\.css$/.test(f)) : [];

        const cssLinkElements = [];
        cssFiles.forEach((file) => {
            cssLinkElements.push(
                <link
                    key={ file }
                    nonce={ this.props.nonce }
                    rel="stylesheet"
                    href={ `${assetPrefix}/_next/${encodeURI(file)}` }
                    crossOrigin={ this.props.crossOrigin }
                />
            );
        });

        return cssLinkElements.length === 0 ? null : cssLinkElements;
    }

    getPreloadMainLinks() {
        return [];
    }

    getPreloadDynamicChunks() {
        return [];
    }
}

export default HeadCustom;
