const toMatch = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i
];

const isMobile = () => toMatch.some((toMatchItem) => {
    return navigator.userAgent.match(toMatchItem);
});

const popupActivate = () => {
    if(typeof window !== 'undefined'){
        window.addEventListener('click', e => {


            if(e.target.dataset?.popupid && !isMobile()){
                const {popupid} = e.target.dataset;
                e.preventDefault();
                const el = document.getElementById(popupid)
                if(el){
                    el.style.display = 'block';
                    el.addEventListener('click', () =>
                        el.style.display = 'none'
                    )
                }
            }


        })
    }
}

export {isMobile}

export default popupActivate
