import {Field} from 'react-final-form';
import {GetBodyValue} from 'Util/LayoutBuilder';
import {FiledRenderPropTypes} from 'Types/Form.proptypes';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css';
import 'Styles/components/Form.scss';

const operators = ['95', '99', '50', '93',
    '96', '98', '67', '97', '63', '66',
    '68', '73', '91', '92', '94', '89'];

const FieldRender = (props) => {
    const className = props.type + ' ' + props.webform_key
    const title = props.title_display !== 'invisible' && <label htmlFor={props.webform_key}>{props.title}</label>
    const fieldData = {
        name: props.webform_key,
        required: props.required
    }
    const error = (meta) => meta.touched && meta.error && <span>{meta.error}</span>

    const renderProcessedText = () => <GetBodyValue value={{processed: props.text}}
                                                    className={className}/>

    const renderText = () => {
        return <Field
            {...fieldData}
            render={({input, meta}) => (
                <div className={className}>
                    {title}
                    <input {...input}
                           placeholder={props.placeholder || (props.title_display === 'invisible' ? props.title : '')}/>
                    {error(meta)}
                </div>
            )}/>
    }

    const renderHidden = () => {
        const {default_value} = props;
        return <Field
            {...fieldData}
            defaultValue={default_value}
            render={({input}) => (
                <input {...input} type="hidden" value={default_value}/>
            )}
        />
    }

    const renderTel = () => {
        return <Field
            {...fieldData}
            render={({input, meta}) => (
                <div className={className}>
                    {title}
                    <PhoneInput
                        placeholder={props.placeholder}
                        disableDropdown={true}
                        disableSearchIcon={true}
                        onlyCountries={['ua']}
                        areaCodes={{ua: operators}}
                        value={input.value || ''}
                        onFocus={e => {
                            // on focus set country code as value.
                            if (!e.target.value) {
                                input.onChange({target: {value: '380', name: input.name}})
                            }
                        }}
                        onBlur={e => {
                            // on focus out clear value if phone number not set.
                            if (e.target.value.length <= 4) {
                                input.onChange({target: {value: '', name: input.name}})
                            }
                        }}
                        onChange={value => {
                            input.onChange({target: {value: value || '380', name: input.name}})
                        }}
                    />
                    {error(meta)}
                </div>
            )}/>
    }

    const renderSubmit = () => {
        return <button type="submit" className={className}>{props.title}</button>
    }

    const fieldsMap = {
        'processed_text': renderProcessedText(),
        'text': renderText(),
        'tel': renderTel(),
        'hidden': renderHidden(),
        'submit': renderSubmit()
    }

    if (fieldsMap[props.type]) return fieldsMap[props.type]
    console.log(props.type)

    return null;
}

FieldRender.propTypes = FiledRenderPropTypes;

export default FieldRender
