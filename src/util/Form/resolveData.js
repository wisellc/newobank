import {sendGetByUrl, sendPostByUrl} from 'Util/Request';
import {sendWithCaptcha} from 'Util/Request/sendWithCaptcha';

function resolveData(fields) {
    const data = [];
    const keys = Object.keys(fields);
    keys.filter(item => item.indexOf('#') === -1).forEach(item => {
            const items = fields[item];
            const field = {};
            const fieldKeys = Object.keys(items);
            fieldKeys.forEach(item => {
                    let value = items[item];
                    const key = item.replace('#', '');
                    if (key === 'type') {
                        if (value === 'textfield') {
                            value = 'text'
                        } else if (value === 'webform_actions') {
                            value = 'submit'
                        } else if (value === 'webform_document_file') {
                            value = 'file'
                        } else if (value === 'webform_select_other') {
                            value = 'select'
                        }
                    }
                    if (key === 'options') {
                        const dataValue = [];

                        const vk = Object.keys(value || {});
                        vk.forEach(ss => dataValue.push({value: ss, label: value[ss]}));
                        value = dataValue;
                    }
                    field[key] = value
                }
            );
            if (field.type === 'text' && field.webform_key === 'phone') {
                field.type = 'tel';
            }

            if (field.type === 'submit') {
                field.title = field.submit__label || field.title
            }

            data.push(field);
        }
    );
    return data;
}

export const validation = (values, fields) => {
    const errors = {}
    fields.forEach(field => {
        if (field.required) {
            if (!values[field.webform_key]) {
                errors[field.webform_key] = __('Obligatory field')
            }
        }
        if (field.maxlength) {
            if (values[field.webform_key]?.length > field.maxlength) {
                errors[field.webform_key] = __('Allowed less than %s charts', field.maxlength)
            }
        }
        if (field.minlength) {
            if (values[field.webform_key]?.length < field.minlength) {
                errors[field.webform_key] = __('Allowed more than %s charts', field.minlength)
            }
        }
    })
    return errors
}

export const formSubmit = (values,
                           callback = () => {
                           },
                           error = () => {
                           }
) => {
    const url = process.env.REACT_APP_API_URL + '/' + getLang() + '/wise/reactapi/webform'

    sendWithCaptcha(token => {
        const body = {
            url: window.location.pathname,
            ...values,
            captchaToken: token
        };

        sendPostByUrl(url, JSON.stringify(body)).then(callback, error)
    })
}

export const sendPhone = (phone,
                          callback = () => {
                          },
                          error = () => {
                          }) => {
    const getPhone = () => phone.substring(2, phone.length);

    sendWithCaptcha(token => {
        const url = process.env.REACT_APP_API_URL + '/' + getLang() +
            '/jsonapi/wise/send-sms-api-bank?onlysms=' + getPhone() +
            '&token=' + token
        sendGetByUrl(url).then(callback, error)
    })
}

export default resolveData
