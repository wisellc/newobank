import fetch from 'isomorphic-unfetch';

global.Headers = global.Headers || require('fetch-headers');
const headers = new Headers();
headers.append('Content-Type', 'application/json');
if(process.env.HTTP_BASE_AUTH){
    headers.set('Authorization', process.env.HTTP_BASE_AUTH);
}

export function handleResponse(response, alias) {
    try {
        return response.json()
    } catch (e){
        return response.text()
    }
    // return response.text().then(text => {
    //     let data = text;
    //     if (data) {
    //         try {
    //             data = JSON.parse(text);
    //         } catch (e) {
    //             console.error(e);
    //             console.warn(text);
    //             data = {}
    //         }
    //     }
    //     return data;
    // });
}



/**
 *
 * @param res
 */
export function fail(res) {
    console.error(res);
}

/**
 *
 * @param result
 * @param type
 * @param name
 * @returns {{result: *, name: *, type: *}}
 */
export function success(result, type, name) {
    return {type: type, result, name}
}

export async function sendGetByUrl(url, isAlias) {
    const requestOptions = {
        method: 'get',
        headers,
    };
    return await fetch(url, requestOptions).then(res => handleResponse(res, isAlias))
}

export function sendPostByUrl(url, body, customMethod) {
    const requestOptions = {
        method: customMethod || 'post',
        headers,
        body
    };
    return fetch(url, requestOptions).then(handleResponse)
}
