export const sendWithCaptcha = (callBack) => {
    const {grecaptcha} = window;
    if(grecaptcha){
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcuUj8gAAAAAHOy3nhzG5ULbL8jws-eoimbOFnS', {action: 'submit'})
                .then(callBack);
        });
    } else {
        console.warn('Recaptcha not loaded')
    }
}
