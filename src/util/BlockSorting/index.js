import Basic from 'Components/Basic';
import Lending from 'Components/Lending';
import Table from 'Components/Table';
import ParagrafBlok from 'Components/ParagrafBlok';
import InfoBlockPopup from 'Components/InfoBlockPopup';
import Bigblock from 'Components/Bigblock';
import Accordion from 'Components/Accordion';
import AccordionAndFiles from 'Components/AccordionAndFiles';
import LendingSense from 'Components/LendingSense';
import Tabs from 'Components/Tabs';
import PoluchitSsylku from 'Components/PoluchitSsylku';
import Calculator from 'Components/Calculator';
import InfoBlockImage from 'Components/InfoBlockImage';
import BanerV2 from 'Components/BanerV2';
import PopupFilesBlock from 'Components/PopupFilesBlock';

export const disabledTitle = [
    'bigblock',
    'lending_sense',
    'calculator',
    'info_block_image',
    'popup_files_block'
]
/* eslint-disable react/prop-types */ // TODO: Update prop-types
const BlockSorting = ({config, data, included}) => {
    const {type} = config;

    const blocksMap = {
        'lending': Lending,
        'basic': Basic,
        'table': Table,
        'paragraf_blok': ParagrafBlok,
        'info_block_popup': InfoBlockPopup,
        'bigblock': Bigblock,
        'accordion': Accordion,
        'accordion_and_files': AccordionAndFiles,
        'lending_sense': LendingSense,
        'tabs': Tabs,
        'poluchit_ssylku': PoluchitSsylku,
        'calculator': Calculator,
        'info_block_image': InfoBlockImage,
        'popup_files_block': PopupFilesBlock,
        'baner_v2': BanerV2
    }

    if (blocksMap[type]) {
        const Block = blocksMap[type];
        return <Block data={data} included={included}/>
    } else {
        console.log(type)
        return ''
    }

}

export default BlockSorting;
