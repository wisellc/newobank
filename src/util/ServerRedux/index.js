export const isClient = () => Boolean(typeof document !== 'undefined' && document.createComment);

export const getServerProps = () => {
    if (isClient()) {
        return JSON.parse(document.getElementById('__NEXT_DATA__').innerHTML);
    }
    return {};
};

export const getServerPageProps = () => getServerProps()?.props?.pageProps || {};

export const getServerStore = () => getServerPageProps().serverStore || {};
