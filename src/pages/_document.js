import Document, {Head, Html, Main, NextScript} from 'next/document'
import HeadCustom from 'Util/Next/headCustom';

class MyDocument extends Document {
    render() {
        const isBot = this.props.__NEXT_DATA__.props.pageProps.isBot;
        const HeadTag = isBot ? HeadCustom : Head
        return (
            <Html lang={getLang()}>
                <HeadTag>
                    <link rel="shortcut icon" href="/favicon.ico"/>
                    <meta name="theme-color" content="#9c386e"/>
                    <meta name="apple-mobile-web-app-capable" content="yes"/>
                    <meta name="apple-mobile-web-app-title" content="Obank"/>
                    <meta name="apple-mobile-web-app-status-bar-style" content="#9c386e"/>
                    <link rel="apple-touch-icon" href="/icons/ios/apple-touch-icon.png"/>
                    <link rel="apple-touch-icon" sizes="57x57"
                          href="/icons/ios/apple-touch-icon-57x57.png"/>
                    <link rel="apple-touch-icon" sizes="72x72"
                          href="/icons/ios/apple-touch-icon-72x72.png"/>
                    <link rel="apple-touch-icon" sizes="76x76"
                          href="/icons/ios/apple-touch-icon-76x76.png"/>
                    <link rel="apple-touch-icon" sizes="114x114"
                          href="/icons/ios/apple-touch-icon-114x114.png"/>
                    <link rel="apple-touch-icon" sizes="120x120"
                          href="/icons/ios/apple-touch-icon-120x120.png"/>
                    <link rel="apple-touch-icon" sizes="144x144"
                          href="/icons/ios/apple-touch-icon-144x144.png"/>
                    <link rel="apple-touch-icon" sizes="152x152"
                          href="/icons/ios/apple-touch-icon-152x152.png"/>
                    <link rel="apple-touch-icon" sizes="180x180"
                          href="/icons/ios/apple-touch-icon-180x180.png"/>
                    <link rel="manifest" href="/manifest.json"/>
                    {!isBot && <script
                        src="https://www.google.com/recaptcha/api.js?render=6LcuUj8gAAAAAHOy3nhzG5ULbL8jws-eoimbOFnS"
                    />}
                    <script dangerouslySetInnerHTML={{__html:
                            '  (function(i,s,o,g,r,a,m){\n' +
                            '  i["esSdk"] = r;\n' +
                            '  i[r] = i[r] || function() {\n' +
                            '    (i[r].q = i[r].q || []).push(arguments)\n' +
                            '  }, a=s.createElement(o), m=s.getElementsByTagName(o)[0]; a.async=1; a.src=g;\n' +
                            '  m.parentNode.insertBefore(a,m)}\n' +
                            '  ) (window, document, "script", "https://esputnik.com/scripts/v1/public/scripts?apiKey=eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI0NTI0ZWZhYTJkYzI2MGRmYTM4YTE1NDBlMWFlYmU1M2QzYmUzNTUwMmU1OGRmODcwMmE1NTY3ZWYyOTQ0ODBhNjhhMmZjMzUwMmEyYWM2MjZmNDU3YWY5YzcyMWM3MGQwOGU4Yzg1NzQxM2E3MWJkYjUzODQzOGQ3MjE4ZmMxNWUzYjNmYjI2M2M3ZDY3NTFhMDYzYWMzNGY2ZDgxZTQ1MTU4MWU1OTI5OTE4NjI1OGZhMGJhYjk1OThhYThiY2IifQ.Qa4ZkIZK7aQViD5GXE55U8tpZT37SC2GFZUyDnab58WzEd-ny4dj0FEWjgax3MtAPEhltbifxjKu4ym28jfCwA&domain=96146AA6-83DE-4EFB-AF96-03CE07EF6331", "es");\n' +
                            '  es("pushOn");\n'}}/>
                    {!isBot && <link rel="stylesheet" href="/fonts.css"/>}
                    <script dangerouslySetInnerHTML={{__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-T2HWFPWP');`}}/>
                </HeadTag>
                <Main/>
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2HWFPWP"
                                  height="0" width="0" style={{display:'none',visibility:'hidden'}} title='googletagmanager'/></noscript>
                 {!isBot && <NextScript/>}
            </Html>
        )
    }
}

export default MyDocument
