import {getFullPageSSR} from 'Store/Page/page.services';
import App from 'Components/App';
import configureStore, {getStore} from 'Store/index';
import {updatePage} from 'Store/Page/page.actions';
import LangDetector from '../../config/SiteLang/LangDetector';
import isbot from 'isbot';
import {isProxyUrl, proxy} from '../../config/Server/nextProxy';
import {getUrlPramsString} from 'Util/Url';

const setStatusCode = (page, res) => {
    if (page && res) {
        if (page.error || !(page.node?.attributes?.status)) {
            res.statusCode = 404
        }
    }
}
App.getInitialProps = async (ctx) => {
    const {asPath, res, req} = ctx;
    LangDetector.set(asPath);
    let userAgent;
    const params = getUrlPramsString(asPath);
    if (typeof window !== 'undefined') {
        if(asPath === '/'){
            window.location.href = '/uk' + params
        }
        userAgent = window.navigator.userAgent;
    } else {
        if(asPath === '/'){
            res.writeHead(301, {location: 'uk' + params});
            res.end();
        }
        if (isProxyUrl(asPath) && res) {
            await proxy(asPath, res);
        }
        userAgent = req?.headers?.['user-agent'] || ''
    }
    const isBot = isbot(userAgent)
    const store = configureStore();
    const serverPage = await getFullPageSSR(asPath);
    if(serverPage.lang_aliases){
        const pathname = asPath.split('?')[0].split('#')[0];
        const newUrl = serverPage.lang_aliases[getLang(asPath)]
        if(newUrl !== pathname){
            if (typeof window !== 'undefined') {
                window.location.href = newUrl
            } else {
                res.writeHead(301, {location: newUrl});
                res.end();
            }
        }
    }
    store.dispatch(updatePage(serverPage))
    const serverStore = getStore().getState()
    setStatusCode(serverPage, res)
    return {
        asPath,
        isBot,
        serverStore,
        userAgent
    }
}

export default App
