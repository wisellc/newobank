import {getUrlPramsString} from 'Util/Url';

const index = () => {};
index.getInitialProps = ({asPath, res}) => {
    if (res) {
        const params = getUrlPramsString(asPath);
        res.writeHead(301, {Location: 'uk' + params});
        res.end();
    }
    return {};
};
export default index;
