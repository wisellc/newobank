import Link from 'next/link';
import 'Styles/scanqr';
import Head from 'next/head';

const IMG_PATH = '/assets/img/static/';

const Index = () => {

    const renderHeader = () => (
        <div className={'sc_header'}>
            <Link href={'/uk'}>
                <img src={IMG_PATH + 'logo_g_b.svg'} width="68px" height="24px" alt={'ideabank'}/>
            </Link>
            <a href={'https://ideabank.ua/uk'}>
                <img src={IMG_PATH + 'idea_logo.svg'} width="83px" height="12px" alt={'ib'}/>
            </a>
        </div>
    )

    const renderMain = () => (
        <div className={'sc_main'}>
            <span>Це посилання потрібно відкривати у додатку O.Bank 2.0</span>
        </div>
    )

    const renderButtons = () => (
        <div className={'sc_buttons'}>
            <a href={'https://apps.apple.com/ua/app/o-bank-2-0/id1585185939?l=ru'}>
                <img src={IMG_PATH + 'App Store.svg'} width="111" height="32" alt={'apple'}/>
            </a>
            <a href={'https://play.google.com/store/apps/details?id=ua.ideabank.obank'}>
                <img src={IMG_PATH + 'Group.svg'} width="111" height="32" alt={'google'}/>
            </a>
        </div>
    )


    return (
        <>
            <div className={'sc_wrapper'}>
                {renderHeader()}
                {renderMain()}
                {renderButtons()}
            </div>
            <Head>
                <style dangerouslySetInnerHTML={{__html: 'body {padding-top: 0;}'}}/>
            </Head>
        </>
    )
}

export default Index
