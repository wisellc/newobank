import 'Styles/main';
import {MyAppPropTypes} from 'Types/index';

function MyApp({Component, pageProps}) {
    return <Component {...pageProps} />
}

MyApp.propTypes = MyAppPropTypes
export default MyApp
