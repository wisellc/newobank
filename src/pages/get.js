const index = () => {
};
index.getInitialProps = ({res, req}) => {
    if (res) {
        const userAgent = req?.headers?.['user-agent'] || '';
        let downloadBankStoreURL = false;
        let userDeviceArray = [
            {device: 'Android', platform: /Android/},
            {device: 'iOS', platform: /iPad|iPhone|iPod/}
        ];

        const getPlatform = userDeviceArray.find(i => i.platform.test(userAgent))?.device
        if (getPlatform === 'Android')
            downloadBankStoreURL = 'https://play.google.com/store/apps/details?id=com.solanteq.obank&amp;utm_source=lending&amp;utm_medium=button';
        if (getPlatform === 'iOS')
            downloadBankStoreURL = 'https://apps.apple.com/ua/app/o-bank-%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%B1%D0%B0%D0%BD%D0%BA/id1490686736?utm_source=lending&amp;utm_medium=button'

        if (downloadBankStoreURL) {
            res.writeHead(302, {Location: downloadBankStoreURL});
        } else {
            res.writeHead(302, {Location: 'uk'});
        }
        res.end();
    }

    return {};
};
export default index;
