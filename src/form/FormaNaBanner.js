import resolveData, {formSubmit, sendPhone, validation} from 'Util/Form/resolveData';
import FieldRender from 'Util/Form/FieldRender';
import {Form} from 'react-final-form'
import {FormaNaBannerPropTypes} from 'Types/Form.proptypes';
import IsPopup from 'Components/Popup/isPopup';
import {connect} from 'react-redux';
import {activatePopup, disablePopup} from 'Store/Popup/popup.actions';

const FormaNaBanner = ({data: {data, form_element}, buttonText, isPopup, dispatch}) => {
    if (!form_element) return null;
    const {webform_id} = data;
    const formattedData = resolveData(form_element);

    const resultPopup = (message) => {
        return (
            <div className="popup__preview active">
                <div className="popup__wrapper">
                    <div className="popup__inner">
                        <div className="popup_close" onClick={() => dispatch(disablePopup())}/>
                        <div className="popup__images">
                            <img alt="" className="popup__logo" src="/sites/default/files/2020-12/logo__popup.png"/>
                            <img alt="" className="popup__img"
                                 src="/sites/default/files/2020-12/cancellation__popup.png"/>
                        </div>
                        <div className="popup__text">{message}</div>
                        <button onClick={() => dispatch(disablePopup())} className="popup__btn">{__('Thanks')} </button>
                    </div>
                </div>
            </div>
        )
    }

    const onSubmit = (e) => {
        const values = {
            ...e,
            webform_id
        }

        const sendForm = () => formSubmit(values,
            res => {
                dispatch(disablePopup())
                dispatch(activatePopup('FormaNaBannerPopup', resultPopup(res.message)));
            },
            error => {
                dispatch(disablePopup())
            }
        )
        if (e.only_sms && e.phone) {
            sendPhone(e.phone,
                ({type, messageExt}) => {
                    if (type === 'SUCCESS') {
                        sendForm()
                    } else {
                        dispatch(activatePopup('FormaNaBannerPopup', resultPopup(messageExt)));
                        dispatch(disablePopup())
                    }
                }, () => {
                    dispatch(disablePopup())
                })
        } else {
            sendForm()
        }
    }

    const form = () => {
        return <Form
            onSubmit={onSubmit}
            validate={(values) => {
                return validation(values, formattedData)
            }}
            render={({handleSubmit}) => (
                <form onSubmit={handleSubmit} className={'form ' + webform_id} id={webform_id}>
                    <div className="popup_close" onClick={() => dispatch(disablePopup())}/>
                    {formattedData.map(props => <FieldRender key={props.webform_key} {...props}/>)}
                </form>
            )}
        />
    }
    if (isPopup) {
        return (
            <IsPopup title={buttonText || __('Open form')}
                     name={webform_id}
                     className={'button'}
            >
                {form()}
            </IsPopup>
        )
    }

    return form()
}

FormaNaBanner.propTypes = FormaNaBannerPropTypes

export default connect()(FormaNaBanner)
