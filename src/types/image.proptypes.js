import {arrayOf, oneOfType, shape, string} from 'prop-types';

const ImagePropTypes = {
    ImagePropTypes:() => ({
        data: oneOfType([
            arrayOf(this.ImgPropTypes),
            this.ImgPropTypes
        ]),
        className: string
    }),
    ImgPropTypes: shape({
        id: string,
        meta: shape({
            alt: string,
            title: string
        }),
        attributes: shape({
            uri: shape({
                url: string
            })
        })
    })
}

export default ImagePropTypes
