import {arrayOf, shape, string} from 'prop-types';

const MetaPropTypes = {
    metatag_normalized: arrayOf(shape({
        attributes: shape({
            name: string,
            content: string
        })
    })),
    MetaPropTypes: () => ({
        attributes: shape({metatag_normalized: this.metatag_normalized})
    }),
}

export default MetaPropTypes
