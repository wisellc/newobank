import {any, arrayOf, bool, shape, string} from 'prop-types';
import MixinsPropTypes from 'Types/mixins.proptypes';
import ImagePropTypes from 'Types/image.proptypes';

const ParagraphPropTypes = {
    AdvantagesPropTypes: {
        attributes: shape({
            field_title: string,
            field_subtitle: string
        }),
        type: string
    },
    InfoBlokPopupPropTypes: {
        attributes: shape({
            field_title: string,
            field_text_paragraph: MixinsPropTypes.BodyPropTypes
        })
    },
    ProductsTabsPropTypes: {
        data: arrayOf(shape({
            type: string,
            id: string
        })),
        type: string
    },
    TabsPropTypes: {
        active: bool,
        relationships: shape({
            field_tabs: shape({
                data: arrayOf(any)
            })
        })
    },
    TekstKartinkaPropTypes: {
        attributes: shape({
            field_text_paragraph: MixinsPropTypes.BodyPropTypes
        }),
        relationships: shape({
            field_image_paragraph: shape({
                data: ImagePropTypes.ImgPropTypes
            })
        }),
        type: string
    },
    LogoLendingPropTypes: {
        attributes: shape({
            field_title: string
        }),
        relationships: shape({
            field_image_paragraph: shape({
                data: ImagePropTypes.ImgPropTypes
            })
        }),
        type: string
    },
    TextAreaPropTypes: {
        attributes: shape({
            field_text_paragraph: MixinsPropTypes.BodyPropTypes
        }),
        type: string
    }
}

export default ParagraphPropTypes;
