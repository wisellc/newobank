import {any, arrayOf, bool, element, func, object, oneOfType, shape, string} from 'prop-types';
import {HEADER_MENU_1, HEADER_MENU_2} from 'Components/Header/Header.config';
import {FOOTER_BLOCK_1, FOOTER_MENU_1, FOOTER_MENU_2, FOOTER_MENU_3} from 'Components/Footer/Footer.config';
import MetaPropTypes from 'Types/meta.proptypes';
import BlockPropTypes from 'Types/block.proptypes';
import MenuPropTypes from 'Types/menu.proptypes';
import MixinsPropTypes from 'Types/mixins.proptypes';

export const LinkPropTypes = {
    children: oneOfType([element, string]),
    href: string,
    className: string,
    onClick: func,
    title: string,
    url: string,
    options: any
}

export const GetBodyValuePropTypes = {
    value: MixinsPropTypes.BodyPropTypes,
    className: string
}

export const AppPropTypes = {
    node: shape({
        attributes: shape({
            layout_builder__layout: arrayOf(shape({
                layout_id: string,
                components: object
            })),
            field_show_title: bool,
            metatag_normalized: MetaPropTypes.metatag_normalized,
            title: string
        }).isRequired
    })
}

export const MyAppPropTypes = {
    Component: func,
    pageProps: shape({
        asPath: string,
        serverStore: object,
        userAgent: string
    })
}

export const WithBlockTitlePropTypes = {
    children: element,
    config: shape({
        id: string,
        className: string,
        field_block_title: string,
        type: string
    })
}

export const HeaderPropTypes = {
    isScrolled: bool,
    isActive: bool,
    handleToggle: func,
    header: shape({
        [HEADER_MENU_1]: arrayOf(shape(MenuPropTypes.MenuItemsProps)),
        [HEADER_MENU_2]: arrayOf(shape(MenuPropTypes.MenuItemsProps))
    })
}

export const FooterPropTypes = {
    footer: shape({
        [FOOTER_BLOCK_1]: BlockPropTypes.BasicPropTypes.data,
        [FOOTER_MENU_1]: arrayOf(shape(MenuPropTypes.MenuItemsProps)),
        [FOOTER_MENU_2]: arrayOf(shape(MenuPropTypes.MenuItemsProps)),
        [FOOTER_MENU_3]: arrayOf(shape(MenuPropTypes.MenuItemsProps))
    })
}
