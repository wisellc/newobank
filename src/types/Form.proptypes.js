import {any, shape, string} from 'prop-types';

export const FiledRenderPropTypes = {
    type: string.isRequired,
    webform_key: string.isRequired,
    title: any,
    title_display: string
}

export const FormaNaBannerPropTypes = {
    data: shape({
        data: shape({webform_id: string.isRequired}),
        form_element: any
    }),
    buttonText: string,
    isPopup: any
}
