import {any, bool, func, oneOfType, shape, string} from 'prop-types';

const MixinsPropTypes = {
    BodyPropTypes: shape({
        processed: string,
        format: string,
        value: string
    }),
    PopupContainerPropTypes: shape({
        popup: oneOfType([string, bool]),
        data: any,
        dispatch: func.isRequired
    }).isRequired,
    PopupComponentPropTypes: shape({
        popup: oneOfType([string, bool]),
        children: any,
        close: func
    }).isRequired
}

export default MixinsPropTypes
