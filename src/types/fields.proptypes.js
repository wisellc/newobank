import {any, arrayOf, objectOf, oneOfType, shape, string} from 'prop-types';
import ImagePropTypes from 'Types/image.proptypes';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const FieldsPropTypes = {
    field_table: shape({
        value: oneOfType([
            arrayOf(string),
            objectOf(any)
        ]),
        caption: string
    }),
    field_info_blok_popup: shape({
        data: shape(ParagraphPropTypes.InfoBlokPopupPropTypes),
        id: string,
        type: string
    }),
    field_paragraf_block: shape({
        data: arrayOf(shape({
            id: string,
            type: string
        }))
    }),
    field_icon: shape({
        data: shape({
            relationships: shape({
                field_ikon: shape({
                    data: ImagePropTypes.ImgPropTypes
                })
            })
        })
    })
}

export default FieldsPropTypes
