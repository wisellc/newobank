import {arrayOf, bool, number, shape, string} from 'prop-types';

export const FileComponentPropTypes = {
    meta: shape({
        description: string,
        display: bool
    }),
    id: string,
    type: string,
    data: shape({
        attributes: shape({
            uri: shape({
                url: string
            }),
            filemime: string,
            filesize: number,
            filename: string
        })
    })
}

export const FileParagraphPropTypes = {
    type: string,
    relationships: shape({
        field_file: shape({data: arrayOf(shape(FileComponentPropTypes))})
    }),
    data: arrayOf(shape(FileComponentPropTypes))
}


