import {any, arrayOf, shape, string} from 'prop-types';
import FieldsPropTypes from 'Types/fields.proptypes';
import MixinsPropTypes from 'Types/mixins.proptypes';
import ImagePropTypes from 'Types/image.proptypes';
import {FormaNaBannerPropTypes} from 'Types/Form.proptypes';

const BlockPropTypes = {
    BigblockPropTypes: {
        data: shape({
            attributes: shape({
                body: MixinsPropTypes.BodyPropTypes,
                field_subtitle: string,
                field_block_title: string,
                field_knopka_popup:any,
                field_popup_form:any
            }),
            relationships: shape({
                field_image: shape({data: ImagePropTypes.ImgPropTypes}),
                field_forma_na_banner: shape(FormaNaBannerPropTypes)
            })
        }),
    },
    PoluchitSsylkuPropTypes: {
        data: shape({
            attributes: shape({
                body: MixinsPropTypes.BodyPropTypes
            }),
            relationships: shape({
                field_forma_na_banner: shape(FormaNaBannerPropTypes)
            })
        }),
    },
    TabsPropTypes: {
        data: shape({
            relationships: shape({
                field_tabs: shape({
                    data: arrayOf(shape({
                        id: string
                    }))})})
        })
    },
    LendingSensePropTypes: {
        data: shape({
            relationships: shape({
                field_paragraf_block: shape({
                    data: arrayOf(any)
                })
            })
        })
    },
    AccordionPropTypes: {
        data: shape({
            type: string,
            attributes: shape({
                field_accordion: arrayOf(shape({
                    first: string,
                    second: string
                }))
            })
        }),
    },
    BasicPropTypes: {
        data: shape({
            attributes: shape({
                body: MixinsPropTypes.BodyPropTypes
            })
        }),
        included: any
    },
    InfoBlockPopupPropTypes: {
        data: shape({
            relationships: shape({
                field_info_blok_popup: shape({
                    data: arrayOf(FieldsPropTypes.field_info_blok_popup)
                })
            })
        }).isRequired,
        included: any
    },
    TablePropTypes: {
        data: shape({
            attributes: shape({
                field_table: arrayOf(FieldsPropTypes.field_table)
            })
        })
    },
    ParagrafBlokPropTypes: {
        data: shape({
            attributes: shape({
                body: MixinsPropTypes.BodyPropTypes
            }),
            relationships: shape({
                field_paragraf_block: FieldsPropTypes.field_paragraf_block
            })
        }),
        included: any
    },
    LendingPropTypes: {
        data: shape({
            attributes: shape({
                body: MixinsPropTypes.BodyPropTypes
            }),
            relationships: shape({
                field_paragraf_block: FieldsPropTypes.field_paragraf_block
            })
        }),
        included: any
    }
}

export default BlockPropTypes
