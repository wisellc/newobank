import {any, arrayOf, bool, element, func, number, oneOfType, shape, string} from 'prop-types';

const MenuItemsProps = {
    weight: oneOfType([string, number]),
    has_children: bool,
    link: any
}

export const MenuPropTypes = {
    MenuItemsProps: {...MenuItemsProps},
    MenuPropTypes: {
        items: arrayOf(shape(MenuItemsProps)),
        id: string,
        navClassName: string,
        ulClassName: string,
        liClassName: string,
        menuHeadingClassName: string,
        menuHeading: string,
        customLink: element,
        onLinkClick: func
    }
}

export default MenuPropTypes
