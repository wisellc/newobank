import '../../styles/components/banerv2.scss';
import Image from 'Components/Image';
import {isMobile} from 'Util/Сrutches/popup.activate';
import {arrayOf, bool, shape, string} from 'prop-types';
import {activatePopup, disablePopup} from 'Store/Popup/popup.actions';
import {useDispatch} from 'react-redux';
import ImagePropTypes from 'Types/image.proptypes';
import FieldsPropTypes from 'Types/fields.proptypes';

const IMG_PATH = '/assets/img/svg/';

export default function BanerV2({data}) {
    const {attributes: {field_form_title, field_table, field_links, field_anons}, relationships: {field_image}} = data;
    const dispatch = useDispatch();

    function renderPopUp() {
        return (
            <div className={'baner_v2_qr'}>
                <div className={'baner_v2_qr-close'} onClick={() => dispatch(disablePopup())}>
                    <img src={IMG_PATH + 'ic_cancel.svg'} alt={'close'}/>
                </div>
                <span className={'baner_v2_qr-title'}>Скануйте та відкривайте депозит у мобільному O.Bank 2.0</span>
                <img src={IMG_PATH + 'qr.png'} className={'baner_v2_qr-image'} alt={'qr code'}/>
            </div>
        )
    }

    function renderButtons() {
        const [{uri, title}] = field_links;
        if (field_anons) {
            return (
                <div className={'baner_v2-links'}>
                    <div onClick={() => {
                        if (isMobile()) {
                            window.open(uri, '_blank').focus()
                        } else {
                            dispatch(activatePopup('qr', renderPopUp()))
                        }
                    }} className={'baner_v2-link'}>
                        {title}
                    </div>
                    <a href={field_links[1].uri} className={'baner_v2-link--more'}>{field_links[1].title}</a>
                </div>
            )
        }
        return (
            <>
                <div onClick={() => {
                    if (isMobile()) {
                        window.open(uri, '_blank').focus()
                    } else {
                        dispatch(activatePopup('qr', renderPopUp()))
                    }
                }} className={'baner_v2-link'}>
                    {title}
                </div>
                <div className={'baner_v2-mobile'}>
                    <img src={IMG_PATH + 'appStore.svg'} alt={'google'}/>
                    <img src={IMG_PATH + 'googlePlay.svg'} alt={'apple'}/>
                    <img src={IMG_PATH + 'huawei.svg'} alt={'huawei'}/>
                </div>
            </>

        )
    }

    function renderTable() {
        const list = Object.values(field_table[0].value);
        const list2 = Object.values(field_table[1].value);
        const renderValue = (v) => {
            if (v === 'true') {
                return <div><img src={IMG_PATH + 'ic_check.svg'} width="32px" height="32px" alt={v}/></div>
            } else if (v === 'false') {
                return <div><img src={IMG_PATH + 'ic_cancel.svg'} width="32px" height="32px" alt={v}/></div>
            }
            return <span>{v}</span>
        }

        const sortAndRender = (l) => l
            .sort((a, b) => parseInt(a.weight) < parseInt(b.weight) ? -1 : 1)
            .map((a) => {
                if (!a[0]) {
                    return null
                }
                return (
                    <li key={a.weight + a[0]}>
                        <strong>{a[0]}</strong>{renderValue(a[1])}
                    </li>
                )
            })

        return (
            <ul className={'list_table'}>
                {sortAndRender(list)}
                {sortAndRender(list2)}
            </ul>
        )
    }

    const renderTitle = (t) => {
        if (field_anons) {
            return <span className={'baner_v2--title'}>{t}</span>
        }
        return <h1>{t}</h1>
    }

    return (
        <div className={'container' + (field_anons ? ' anons' : '')}>
            <Image data={field_image.data} webp={field_image.webp} disableLazy={true} className={'baner_v2--images'}/>
            <div className={'baner_v2--content'}>
                {renderTitle(field_form_title)}
                {renderTable()}
                {renderButtons()}
            </div>
        </div>
    )
}

BanerV2.propTypes = {
    data: shape({
        attributes: shape({
            field_form_title: string.isRequired,
            field_anons: bool,
            field_links: arrayOf(shape({
                uri: string.isRequired,
                title: string.isRequired
            })).isRequired,
            field_table: arrayOf(FieldsPropTypes.field_table).isRequired
        }).isRequired,
        relationships: shape({
            field_image: shape({data: ImagePropTypes.ImgPropTypes.isRequired})
        })
    })
}
