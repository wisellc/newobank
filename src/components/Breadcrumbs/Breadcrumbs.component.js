import {useSelector} from 'react-redux';
import Link from 'next/link';
import 'Styles/components/_breadcrumbs.sass'

const BreadcrumbsComponent = () => {
    const {breadcrumbs} = useSelector(state => state.pageReducer.page)
    const origin = typeof window !== 'undefined' ? window.location.origin : 'https://ideabank.ua';
    if (breadcrumbs.length <= 1) return null;
    return (
        <div className='container'>
            <ul className={'breadcrumbs'}
                itemScope
                itemType="https://schema.org/BreadcrumbList">
                {breadcrumbs.map((item, index) => <li key={item.url + item.text}
                                                      className='breadcrumbs__item'
                                                      itemProp="itemListElement"
                                                      itemScope
                                                      itemType="https://schema.org/ListItem">
                    {item.url && item.url.indexOf('<none>') === -1 ? <Link href={item.url}>
                        <a itemProp="item" href={item.url}>
                            <meta itemProp="item" content={origin + (item.url || '')}/>
                            <span itemProp="name">{item.text}</span>
                        </a></Link> : <span itemProp="name">{item.text}</span>}
                    <meta itemProp="position" content={index}/>
                </li>)}
            </ul>
        </div>
    )
}

export default BreadcrumbsComponent;
