import 'Styles/components/footer';
import Menu from 'Components/Menu';
import Basic from 'Components/Basic';
import {FooterPropTypes} from 'Types/index';
import {
    FOOTER_BLOCK_1,
    FOOTER_BLOCK_2,
    FOOTER_MENU_1,
    FOOTER_MENU_2,
    FOOTER_MENU_3
} from 'Components/Footer/Footer.config';
import IsPopup from 'Components/Popup/isPopup';
import File from 'Paragraph/File';
import FooterHtmlBlock from 'Components/Footer/Footer.htmlBlock';

function Footer({footer}) {
    return (
        <footer className="footer">
            <div className="container --containerLarge ">
                <div className="footer__wrap">
                    <div className="footer__brand">
                        <Basic data={footer[FOOTER_BLOCK_1]}/>
                    </div>
                    <div className="footer__wrap-item">
                        <Menu id={FOOTER_MENU_1}
                              items={footer[FOOTER_MENU_1]}
                              navClassName={'footer__item'}
                              menuHeading={__('Bank')}
                              menuHeadingClassName={'footer__item-title'}
                              ulClassName={'footer__menu-list'}
                              additionalRender={() =>
                                  <IsPopup title={__('Information for survivors')}
                                           name={'footer'}
                                           className={'no-route-link'}
                                  >
                                      <div className={'footer__modal-title'}>
                                          <h3>{__('Information for survivors')}</h3>
                                      </div>
                                      <File {...footer[FOOTER_BLOCK_2]}/>
                                  </IsPopup>
                              }
                        />
                        <Menu id={FOOTER_MENU_3}
                              items={footer[FOOTER_MENU_3]}
                              navClassName={'footer__item'}
                              menuHeading={__('Products')}
                              menuHeadingClassName={'footer__item-title'}
                              ulClassName={'footer__menu-list'}
                        />
                        <Menu id={FOOTER_MENU_2}
                              items={footer[FOOTER_MENU_2]}
                              navClassName={'footer__item'}
                              menuHeading={__('Services')}
                              menuHeadingClassName={'footer__item-title'}
                              ulClassName={'footer__menu-list'}
                        />
                        <FooterHtmlBlock/>
                    </div>
                </div>
            </div>
        </footer>
    );
}

Footer.propTypes = FooterPropTypes

export default Footer;
