import {connect} from 'react-redux';
import Footer from 'Components/Footer/footer';

const mapStateToProps = (state) => ({
    footer: state.pageReducer.page.footer
})

const FooterContainer = (props) => {
    return <Footer
        {...props}
    />
}

export default connect(mapStateToProps)(FooterContainer)
