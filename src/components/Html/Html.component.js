import parser from 'html-react-parser';
import attributesToProps from 'html-react-parser/lib/attributes-to-props';
import domToReact from 'html-react-parser/lib/dom-to-react';
import Link from 'next/link';
import PropTypes from 'prop-types';

const Html = ({ content }) => {
    
    if (!content) {
        return '';
    }
    // eslint-disable-next-line consistent-return
    const replaceLinks = ({ attribs, children }) => {
        const { href } = attribs;
        if (href) {
            const isAbsoluteUrl = (value) => new RegExp('^(?:[a-z]+:)?//', 'i').test(value);
            const isSpecialLink = (value) => new RegExp('^(sms|tel|mailto):', 'i').test(value);

            if (!isAbsoluteUrl(href) && !isSpecialLink(href)) {
                return (
                    <Link { ...attributesToProps(attribs) }>
                        { /* eslint-disable-next-line no-use-before-define */ }
                        { domToReact(children, parserOptions) }
                    </Link>
                );
            }
        }
    };

    const rules = [
        {
            query: { name: ['a'] },
            replace: replaceLinks
        }
    ];

    const parserOptions = {
        // eslint-disable-next-line consistent-return
        replace: (domNode) => {
            const { data, name: domName, attribs: domAttrs } = domNode;
            // Let's remove empty text nodes
            if (data && !data.replace(/\u21b5/g, '').replace(/\s/g, '').length) {
                // eslint-disable-next-line react/jsx-no-useless-fragment
                return <></>;
            }

            const rule = rules.find((rule) => {
                const { query: { name, attribs } } = rule;

                if (name && domName && name.indexOf(domName) !== -1) {
                    return true;
                } if (attribs && domAttrs) {
                    for (let i = 0; i < attribs.length; i++) {
                        const attrib = attribs[i];

                        if (typeof attrib === 'object') {
                            const queryAttrib = Object.keys(attrib)[0];

                            if (Object.prototype.hasOwnProperty.call(domAttrs, queryAttrib)) {
                                return domAttrs[queryAttrib].match(Object.values(attrib)[0]);
                            }
                        } else if (Object.prototype.hasOwnProperty.call(domAttrs, attrib)) {
                            return true;
                        }
                    }
                }

                return false;
            });

            if (rule) {
                const { replace } = rule;
                return replace.call(this, domNode);
            }
        }
    };

    return parser(content, parserOptions);
};

Html.propTypes = {
    content: PropTypes.string
};

Html.defaultProps = {
    content: ''
};

export default Html;
