import {any, arrayOf, shape, string} from 'prop-types';
import 'Styles/components/PopupFilesBlock';
import {useDispatch} from 'react-redux';
import {activatePopup, disablePopup} from 'Store/Popup/popup.actions';
import File from 'Paragraph//File';

const IMG_PATH = '/assets/img/svg/';
export default function PopupFilesBlock({data}) {
    const {
        attributes: {field_block_title, field_knopka_popup},
        relationships: {field_paragraf_block: {data: files}}
    } = data;
    const dispatch = useDispatch();

    function renderPopUp() {
        return (
            <div className={'baner_v2_qr'}>
                <div className={'baner_v2_qr-close'} onClick={() => dispatch(disablePopup())}>
                    <img src={IMG_PATH + 'ic_cancel.svg'} alt={'close'}/>
                </div>
                <span className={'baner_v2_qr-title'}>{field_block_title}</span>
                <div className={'filesBlock__content'}>
                    {files.map(({type, id, data: {relationships: {field_description_file: {data}}}}) => (
                        <React.Fragment key={id}>
                            {data.map(a => (
                                <File
                                    key={a.paragraph_type.data.id}
                                    id={a.paragraph_type.data.id}
                                    relationships={a}
                                    type={type}
                                />
                            ))}
                        </React.Fragment>
                    ))}
                </div>
            </div>
        )
    }

    return (
        <div className={'container PopupFilesBlock'}>
            <span className={'PopupFilesBlock_title'}>{field_block_title}</span>
            <button className={'PopupFilesBlock_button'}
                    onClick={() => dispatch(activatePopup('filesBlock', renderPopUp()))}
            >{field_knopka_popup}</button>
        </div>
    )
}

PopupFilesBlock.propTypes = {
    data: shape({
        attributes: shape({
            field_block_title: string,
            field_knopka_popup: string
        }),
        relationships: shape({
            field_paragraf_block: shape({
                data: arrayOf(shape({
                    type: string,
                    id: string,
                    data: shape({
                        relationships: shape({
                            field_description_file: shape({
                                data: arrayOf(any)
                            })
                        })
                    })
                }))
            })
        })
    })
}
