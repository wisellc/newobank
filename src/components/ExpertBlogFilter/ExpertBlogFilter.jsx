import 'Styles/components/_expertBlogFilter.sass';
import {useRouter} from 'next/router';
import PropTypes from 'prop-types';

const ExpertBlogFilter = ({categoryList = []}) => {
    const router = useRouter();
    const {query: {category, page, ...all}, pathname} = router;

    function filterByItemName(category) {
        router.push({
            pathname,
            query: {
                ...all,
                ...(category !== '0' ? {category} : {})
            }
        })
    }

    return (
        <div className={'scroll_changer category_changer category_changer_blog'}>
            <div className={'scroll_horizontal'}>
                {categoryList.map( ( {id, name}, index ) =>
                    <div key={index + id}
                        onClick={ () => (filterByItemName(id))}
                        className={`category_changer_blog_link ${((category || 0) == id) ?  'active' : ''}`}>
                            {name}
                    </div>
                )}
            </div>
        </div>
    );
}

ExpertBlogFilter.propTypes = {
    categoryList: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string
    }))
}

export default ExpertBlogFilter
