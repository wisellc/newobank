import 'Styles/components/table';
import BlockPropTypes from 'Types/block.proptypes';
import Html from 'Components/Html';

const TableComponent = ({data}) => {
    const {attributes: {field_table}} = data;
    const renderTableContent = (tbody) => {
        return (Object.values(tbody)).map((tr, index) =>
            index !== 0 ?
                <tr key={index}>
                    {Object.values(tr).map((td, tdIndex) => <td key={tdIndex}><Html content={td}/></td>)}
                </tr> : null
        )
    }
    return field_table.map(({value, caption}, index) =>
        <div key={index} className="container --containerTablet ">
            <div className="table__wrap">
                <div className="table__btn"><a href="/sites/default/files/pdf/tarify.pdf" rel="noreferrer"
                                               title="pravila.pdf" target="__blank">{__('Detailed rates')}</a></div>
                {caption && <div className="table__caption">
                    <h3>{caption}</h3>
                </div>}
                <div className="table__table-wrap">
                    <table>
                        <thead>
                        <tr>
                            {value[0].map((heading, index) =>
                                <th key={index}><Html content={heading}/></th>
                            )}
                        </tr>
                        </thead>
                        <tbody>
                        {renderTableContent(value)}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>)
}

TableComponent.propTypes = BlockPropTypes.TablePropTypes

export default TableComponent;
