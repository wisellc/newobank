import {GetBodyValue} from 'Util/LayoutBuilder';
import {getParagraphType, resolveParagraphItems} from 'Paragraph/index';
import Image from 'Components/Image';
import BlockPropTypes from 'Types/block.proptypes';
import 'Styles/components/Bigblock.scss';
import dynamic from 'next/dynamic';
const FormaNaBanner = dynamic(() => import( 'Form/FormaNaBanner'), {ssr: false});

const BigblockComponent = ({data}) => {
    const {
        attributes: {body, field_subtitle, field_block_title, field_knopka_popup, field_popup_form, field_block_view},
        relationships: {field_image, field_logo, field_advantages, field_forma_na_banner},
        type: initialType
    } = data;

    const type = getParagraphType(initialType);
    return <div className={type + '__wrapper ' + (field_block_view ? field_block_view : 'default')}>
        <div className='container --containerPreview'>
            <div className={type + '__image'}>
                <Image data={field_image.data} webp={field_image.webp} disableLazy={true} className={type + '__images'}/>
            </div>
            <div className={type + '__description'}>
                {resolveParagraphItems(field_logo.data)}
                {field_block_title && <h1 className={type + '__title'}>{field_block_title}</h1>}
                {field_subtitle && <span className={type + '__subtitle'}>{field_subtitle}</span>}
                <div className='advantages'>
                    {field_advantages.data.map(resolveParagraphItems)}
                </div>
                {field_forma_na_banner && <FormaNaBanner data={field_forma_na_banner} buttonText={field_knopka_popup}
                                                         isPopup={field_popup_form}/>}
                <GetBodyValue value={body} className={type + '__text'}/>
            </div>
        </div>
    </div>
}

BigblockComponent.propTypes = BlockPropTypes.BigblockPropTypes

export default BigblockComponent
