import {useState} from 'react';
import {useSelector} from 'react-redux';

const LanguageSwitcherComponent = () => {
    const [active, setActive] = useState(false)

    const {lang_aliases} = useSelector(state => state.pageReducer.page)

    const actions = {
        onClick: () => setActive(!active),
        onMouseEnter: () => setActive(true),
        onMouseLeave: () => setActive(false)
    }
    if(!lang_aliases) return null;

    return (
        <div className="header__language" {...actions}>
            <ul className={active ? 'active' : ''}>
                {Object.keys(lang_aliases).map(lang =>
                    <li key={lang} className="header__language-items">
                        <a href={lang_aliases[lang]}
                            className={getLang() === lang ? 'open-lang' : ''}
                        >
                            {__(lang)}
                        </a>
                    </li>
                )}
            </ul>
        </div>
    )

}

export default LanguageSwitcherComponent
