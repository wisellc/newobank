import {resolveParagraphItems} from 'Paragraph/index';
import 'Styles/components/Lending.scss';
import BlockPropTypes from 'Types/block.proptypes';

const LendingComponent = ({data: {relationships: {field_paragraf_block: {data}}}}) => {
    return <div className="container">{data.map(resolveParagraphItems)}</div>
}

LendingComponent.propTypes = BlockPropTypes.LendingPropTypes

export default LendingComponent;
