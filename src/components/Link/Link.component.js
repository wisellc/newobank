import Link from 'next/link';
import {Link as ScrollLink} from 'react-scroll';
import {LinkPropTypes} from 'Types/index';
import {withRouter} from 'next/router';

const LinkComponent = (props) => {
    const {className, onClick, children, url, title, router, options} = props;
    if (!url) return <div className={className + 'no-route-link'} onClick={onClick}>
        {children || title}
        <span title={__('Expected')}/>
    </div>

    const isAnchor = () => url.match(/#/);

    const isExternal = () => url.match(/^https?:\/\//);

    const isExternalAnchor = () => {
        if (isAnchor()) {
            if (options.anchor) return false;
            return !(router.asPath.split('#')[0] === url.split('#')[0])
        }
        return false
    }

    const NextLink = <Link href={url}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a className={className} onClick={onClick} title={title}>
            {children || title}
        </a>
    </Link>

    if (isExternalAnchor()) {
        return NextLink;
    }

    if (isAnchor()) {
        return <ScrollLink
            activeClass="active"
            to={url.split('#')[1]}
            spy={true}
            onClick={onClick}
            smooth={true}
            offset={-70}
            duration={500}
            className={className}
            title={title}
        >
            {children || title}
        </ScrollLink>
    }
    if (isExternal()) {
        return <a className={className} onClick={onClick} href={url} title={title}>
            {children || title}
        </a>
    }

    return NextLink
}

LinkComponent.propTypes = LinkPropTypes

LinkComponent.defaultProps = {
    onClick: () => {
    },
    className: '',
    url: '',
    title: '',
    description: '',
    options: {}
}

export default withRouter(LinkComponent);
