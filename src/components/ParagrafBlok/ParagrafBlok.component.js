import 'Styles/components/ParagrafBlok.scss'
import {resolveParagraphItems} from 'Paragraph/index';
import BlockPropTypes from 'Types/block.proptypes';

const ParagrafBlokComponent = ({data: {relationships: {field_paragraf_block: {data}}}}) => {
    return <div className="container"><div className="kartinka_tekst__inner">{data.map(resolveParagraphItems)}</div></div>
}

ParagrafBlokComponent.propTypes = BlockPropTypes.ParagrafBlokPropTypes

export default ParagrafBlokComponent;
