import Head from 'next/head';
import {useSelector} from 'react-redux';

export default function Meta() {
    const {node: {attributes: {metatag_normalized}}} = useSelector(state => state.pageReducer.page)
    if (!metatag_normalized) return null;
    const title = metatag_normalized.find(({attributes}) => attributes.name === 'title').attributes.content;

    const getLine = ({attributes, tag: Tag}, index) => <Tag key={index} {...attributes}/>;

    return (
        <Head>
            <meta charSet="utf-8"/>
            <title>{title}</title>
            {metatag_normalized.map(getLine)}
        </Head>
    )
}
