import {getParagraphType, resolveParagraphItems} from 'Paragraph/index';
import BlockPropTypes from 'Types/block.proptypes';
import Image from 'Components/Image';
import Slider from 'react-slick';
import {useRef, useState, useEffect , default as React} from 'react';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import 'Styles/components/LendingSense.scss';

const LendingSenseComponent = ({
                                   data: {
                                       relationships: {field_paragraf_block: {data}},
                                       type: wrapperType,
                                       attributes
                                   }
                               }) => {
    const type = getParagraphType(wrapperType);
    let slider = useRef(null);
    const [activeSlide, setActiveSlide] = useState(0);

    useEffect(() => {
        slider.slickGoTo(activeSlide)
    }, [activeSlide])

    const renderText = ({data: {relationships, attributes, type: itemType}}, index) => {
        return <li className={`${type}__item`}
                   key={index}
                   onMouseEnter={() => setActiveSlide(index)}
        >
            <div className={`${getParagraphType(itemType)}__icon`}>
                <Image data={relationships.field_icon.data?.relationships.field_ikon.data}/>
            </div>
            <span className={activeSlide === index ? 'active' : ''}>{attributes.field_title}</span>
            {relationships.field_paragraph.data.map(resolveParagraphItems)}
        </li>
    }

    const renderImages = (item, index) => {
        const {data: {relationships: {field_image_paragraph}}} = item;
        return <React.Fragment key={index}>
            {renderText(item)}
            <Image
                data={field_image_paragraph.data}
                webp={field_image_paragraph.data?.webp}
                className={type + '__images'}
            />
        </React.Fragment>
    }

    const settings = {
        className: type + '__slider',
        dots: !1,
        arrows: !1,
        speed: 400,
        fade: !0,
        swipe: !1,
        draggable: !1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                centerMode: !0,
                fade: !1,
                swipe: !0,
                draggable: !0,
                variableWidth: !0,
                centerPadding: '0px',
                swipeToSlide: !0
            }
        },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                    swipe: true,
                    centerMode: true,
                    draggable: true,
                    fade: false,
                    swipeToSlide: true
                }
            }]
    }

    return (
            <div className='container'>
                <div className={type + '__wrapper'}>
                    <Slider ref={(sl) => slider = sl} {...settings}>
                        {data.map(renderImages)}
                    </Slider>
                    <div className={type + '__content'}>
                        <h3>{attributes.field_block_title}</h3>
                        <span>{attributes.field_subtitle}</span>
                        <ul className={type + '__list'}>
                            {data.map(renderText)}
                        </ul>
                    </div>
                </div>
            </div>
    )
}

LendingSenseComponent.propTypes = BlockPropTypes.LendingSensePropTypes

export default LendingSenseComponent;
