import 'Styles/components/InfoBlockImage';
import {shape, string} from 'prop-types';

export default function InfoBlockImage(props){
    const {data: {attributes: {field_block_title}, relationships: {field_image_title_description: {data}}}} = props;

    const renderListItem = ({data: {attributes: {field_title}}}, index) => {
        return (
            <li key={index}>
                <span className={'info_block_image__icon'}>{index + 1}</span>
                <span className={'info_block_image__description'}>{field_title}</span>
            </li>
        )
    }

    return (
        <div className={'container info_block_image'}>
            <span className={'info_block_image__title'}>{field_block_title}</span>
            <ul>
                {data.map(renderListItem)}
            </ul>
        </div>

    )
}

InfoBlockImage.propTypes = {
    data: shape({
        attributes: {
            field_block_title: string
        }
    })
}
