import 'Styles/components/_socialNetworks.sass';

const SocialNetworks = ( {field_sotsialni_merezhi} ) => {
    if(!field_sotsialni_merezhi){
        return null
    }


    const renderSocialNetworks = ({ uri, title }, index) => (
        <a key={index} href={uri} className={title}></a>
    )

    return (
        <div className="expert__social-networks">
            {field_sotsialni_merezhi.map(renderSocialNetworks)}
        </div>

    );
}

export default SocialNetworks
