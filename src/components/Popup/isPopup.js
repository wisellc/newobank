import {connect} from 'react-redux';
import {activatePopup} from 'Store/Popup/popup.actions';
import {any, arrayOf, element, func, oneOfType, string} from 'prop-types';

const IsPopup = ({title, children, dispatch, name, className}) => {
    return <span className={className + ' popup-activate'} onClick={() => dispatch(activatePopup(name,children))}>{title}</span>
}

IsPopup.defaultProps = {
    className: '',
    name: 'popup',
    children: <div/>,
    title: 'Activate Popup'
}

IsPopup.propTypes = {
    title: any,
    children: oneOfType([element.isRequired, arrayOf(element).isRequired]),
    className: string,
    name: string.isRequired,
    dispatch: func
}

export default connect()(IsPopup)
