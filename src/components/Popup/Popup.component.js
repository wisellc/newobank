import MixinsPropTypes from 'Types/mixins.proptypes';
import 'Styles/components/Popup.scss'

const PopupComponent = ({popup, close, children}) => {
    return <div className={`popup__modal ${popup}__modal active`}>
        <div className={`popup__close-btn ${popup}__close-btn`} onClick={close}/>
        <div className="container">
            <div className={`popup__modal-wrap ${popup}__modal-wrap`}>
                {children}
            </div>
        </div>
    </div>;
}

PopupComponent.propTypes = MixinsPropTypes.PopupComponentPropTypes;

export default PopupComponent
