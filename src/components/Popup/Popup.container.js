import PopupComponent from 'Components/Popup/Popup.component';
import {connect} from 'react-redux';
import {disablePopup} from 'Store/Popup/popup.actions';
import MixinsPropTypes from 'Types/mixins.proptypes';

const PopupContainer = ({popup, data, dispatch}) => {

    const componentProps = {
        popup,
        children: data,
        close: () => dispatch(disablePopup())
    }

    if(!popup) return null;

    return <PopupComponent
        {...componentProps}
    />
}

PopupContainer.propTypes = MixinsPropTypes.PopupContainerPropTypes;

const mapStateToProps = (state) => {
    const {popup, data} = state.popupReducer;
    return {
        popup,
        data
    }
}


export default connect(mapStateToProps)(PopupContainer)
