import 'Styles/components/header';
import Menu from 'Components/Menu';
import Link from 'Components/Link';
import LanguageSwitcher from 'Components/LanguageSwitcher';
import {HeaderPropTypes} from 'Types/index';
import {HEADER_MENU_1 , HEADER_MENU_2} from 'Components/Header/Header.config';

const logo = '/assets/img/svg/logo_g_b.svg';

function Header(props) {
    const {isScrolled, isActive, handleToggle, onClickBtnMenu, header} = props;

    return (
        <header className={`header ${isScrolled ? 'scrolled' : ''}`}>
            <div className="container">
                <div className="header__wrap">
                    <div className={`header__wrap-nav ${isActive ? 'open_nav' : ''}`}>
                        <Menu items={header[HEADER_MENU_1]}
                              id={HEADER_MENU_1}
                              onLinkClick={handleToggle}
                              navClassName={'header__nav'}
                              ulClassName={'header__nav-list'}
                              liClassName={'header__nav-item'}
                        />
                        <div className="header__logo">
                            <Link url={'/' + getLang()} title={'O.Bank'}><img src={logo} alt={'logo'}/></Link>
                        </div>
                        <Menu items={header[HEADER_MENU_2]}
                              id={HEADER_MENU_2}
                              onLinkClick={handleToggle}
                              navClassName={'header__nav'}
                              ulClassName={'header__nav-list'}
                              liClassName={'header__nav-item'}
                        />

                    </div>
                    <LanguageSwitcher/>
                    <div className="header__burger-position">
                        <Link url={'/' + getLang()} title={'O.Bank'}><img src={logo} alt={'logo'}/></Link>
                        <div className={`header__btn-menu ${isActive ? 'active' : ''}`} onClick={onClickBtnMenu}>
                            <span/>
                            <span/>
                            <span/>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

Header.propTypes = HeaderPropTypes

export default Header;
