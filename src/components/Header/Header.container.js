import Header from 'Components/Header/header';
import {connect} from 'react-redux';
import {useEffect, useState} from 'react';
import { toggleScroll } from 'Util/Browser/Browser';

const mapStateToProps = (state) => ({
    header: state.pageReducer.page.header
})

const HeaderContainer = (props) => {
    const [isScrolled, setScrolled] = useState(false);

    const listener = e => {
        const bodyOffset = document.body.getBoundingClientRect();
        if (-bodyOffset.top > 80) {
            setScrolled(true)
        } else {
            setScrolled(false)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', listener);
        return () => {
            window.removeEventListener('scroll', listener);
        };
    });

    const [isActive, setActive] = useState(false);
    const handleToggle = () => {
        setActive(!isActive);
        toggleScroll(true)
    };

    const onClickBtnMenu = ()=> {
        handleToggle()

        if (isActive) {
            toggleScroll(true)
        } else {
            toggleScroll(false)
        }
    }

    const containerFunc = {
        setActive,
        setScrolled,
        handleToggle,
        onClickBtnMenu
    }
    const containerProps = {
        isScrolled,
        isActive
    }

    return <Header
        {...props}
        {...containerFunc}
        {...containerProps}
    />
}


export default connect(mapStateToProps)(HeaderContainer)
