import 'Styles/components/Img.scss';
import {NotFoundImage} from 'Components/Image/index';
import ImagePropTypes from 'Types/image.proptypes';
import {LazyLoadImage} from 'react-lazy-load-image-component';

const ImageComponent = ({data, className = '', webp, disableLazy = false}) => {
    if (webp) {
        return <div className={className} dangerouslySetInnerHTML={{__html: webp}}/>
    }

    if (!data) {
        return null;
    }
    const renderImage = ({meta, attributes, id, data}) => {
        const ImgTag = disableLazy ? 'img' : LazyLoadImage;
        return (
            <ImgTag
                id={id}
                key={id}
                alt={''}
                {...meta}
                className={className}
                src={attributes?.uri?.url || data?.attributes?.uri?.url || NotFoundImage}
            />
        )
    }
    if (data.length) {
        return data.map(renderImage)
    } else {
        return renderImage(data)
    }
}

ImageComponent.propTypes = ImagePropTypes.ImagePropTypes

export default ImageComponent;
