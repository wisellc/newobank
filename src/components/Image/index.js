import Image from './Image.component';

export const NotFoundImage = '/logo192.png'

Image.defaultProps = {
    className: '',
    data: {
        meta: {},
        id: '',
        attributes: {
            uri: {
                url: NotFoundImage
            }
        }
    }
}
export default Image;
