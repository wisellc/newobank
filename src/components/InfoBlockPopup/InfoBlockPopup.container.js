import {getParagraphType, resolveParagraphItems} from 'Paragraph/index';
import 'Styles/components/InfoBlockPopup.scss'
import BlockPropTypes from 'Types/block.proptypes';

const InfoBlockPopupContainer = ({data: {relationships, type: wrapperType}}) => {
    const type = getParagraphType(wrapperType);
    return (
        <div className="container">
            <div className={type + '__wrapper'}>
                {relationships.field_info_blok_popup.data.map(resolveParagraphItems)}
            </div>
        </div>
    )
}

InfoBlockPopupContainer.propTypes = BlockPropTypes.InfoBlockPopupPropTypes

export default InfoBlockPopupContainer;

