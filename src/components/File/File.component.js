import {FileComponentPropTypes} from 'Types/file.propTypes';

const FileComponent = ({meta: {description, display}, id, type, data}) => {
    if(!data) return null;

    const {attributes: {uri, filemime, filesize, filename}} = data;

    const className = (mod) => {
        return `${type}__${mod}`
    }

    return <div className={className('wrapper')} id={id}>
        <span className={className(filemime.split('/')[0]) + ' ' + className(filemime.split('/')[1]?.split('.')[0])}>
            <a href={uri.url}
               rel="noreferrer"
               target={'_blank'}
               type={`${filemime}; length=${filesize}`} title={filename}>{display && description}</a></span>
    </div>
}

FileComponent.propTypes = FileComponentPropTypes

export default FileComponent
