import {getParagraphType, resolveParagraphItems} from 'Paragraph/index';
import {useState} from 'react';
import BlockPropTypes from 'Types/block.proptypes';
import 'Styles/components/Tabs.scss';

const TabsComponent = ({data}) => {
    const {relationships: {field_tabs}, type: initialType} = data;
    const type = getParagraphType(initialType);
    const [activeId, setActive] = useState(field_tabs.data[0].id)

    const tabsHeading = [];
    field_tabs.data.forEach(a => {
        tabsHeading.push({
            label: a.data.attributes.field_title,
            id: a.data.id
        })
    })

    const renderContent = (item, index) => {
        item.data.active = item.id === activeId;
        return <div className={type + '__item'} key={item.id}>
            <div className={type + '__heading-mobile' + (activeId === item.id ? ' active' : '')} onClick={() => {
                setActive(item.id === activeId ? null : item.id)
            }}>
                {item.data.attributes.field_title}
            </div>
            {resolveParagraphItems(item, index)}
        </div>
    }

    return <div className="container">
        <div className={type + '__wrapper'}>
            {(tabsHeading.length > 1) && <div className={type + '__heading'}>
                {tabsHeading.map(a => <div
                    key={a.id}
                    onClick={() => setActive(a.id)}
                    className={activeId === a.id ? 'active' : ''}
                >{a.label}</div>)}
            </div>}
            <div className={type + '__content'}>
                {field_tabs.data.map(renderContent)}
            </div>
        </div>
    </div>

}

TabsComponent.propTypes = BlockPropTypes.TabsPropTypes

export default TabsComponent
