import Link from 'Components/Link';
import {useState} from 'react';
import MenuPropTypes from 'Types/menu.proptypes';

const MenuComponent = ({onLinkClick,items, id, customLink, navClassName, ulClassName, liClassName, menuHeading, menuHeadingClassName, additionalRender}) => {
    const RenderLink = customLink ? customLink : Link;
    const sortMenuItems = (a, b) => parseInt(a.link.weight, 10) < parseInt(b.link.weight, 10) ? -1 : 1;

    const renderChild = (item, level) => {
        if (item.has_children) {
            // eslint-disable-next-line react-hooks/rules-of-hooks
            const [active, setActive] = useState(false);
            const mouseActions = {
                onMouseEnter: () => setActive(true),
                onMouseLeave: () => setActive(false)
            }
            return (
                <>
                    <div className={id + '-parent level_' + level + (active ? ' active' : '')}
                         onClick={() => setActive(!active)}
                         {...mouseActions}
                    >
                        {item.link.title}
                    </div>
                    <ul className={id + '-list ' + ulClassName + ' level_' + level + (active ? ' active' : '')}
                        {...mouseActions}
                    >
                        {item.subtree.sort(sortMenuItems).map((menuChild, childIndex) =>
                            <li key={childIndex} className={id + '-item ' + liClassName + ' level_' + level}>
                                {renderChild(menuChild, level + 1)}
                            </li>)}
                    </ul>
                </>
            )
        }

        return <RenderLink onClick={onLinkClick} {...item.link}/>
    }

    const [opened, openMenu] = useState(false);

    return <nav className={id + ' ' + navClassName}>
        {menuHeading &&
        <span
            className={menuHeadingClassName + (opened ? ' active' : '')}
            onClick={() => openMenu(!opened)}>
            {menuHeading}
        </span>}
        <ul className={id + '-list ' + ulClassName + (menuHeading && opened ? ' active' : '')}>
            {items.sort(sortMenuItems).map((item, index) =>
                <li key={index} className={id + '-item ' + liClassName}>{renderChild(item, 0)}</li>
            )}
            {additionalRender()}
        </ul>
    </nav>
}


MenuComponent.propTypes = MenuPropTypes.MenuPropTypes
MenuComponent.defaultProps = {
    items: [{
        weight: 0
    }],
    additionalRender: () => null,
    onLinkClick: () => null,
    navClassName: '',
    ulClassName: '',
    liClassName: '',
    menuHeadingClassName: ''
}

export default MenuComponent;


