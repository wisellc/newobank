import {useEffect, useState} from 'react';
import {getParagraphType} from 'Paragraph/index';
import BlockPropTypes from 'Types/block.proptypes';
import 'Styles/components/Accordion.scss'

const AccordionComponent = ({data: {attributes: {field_accordion}, type: wrapperType}}) => {
    const type = getParagraphType(wrapperType);
    const [active, setActive] = useState(null);

    useEffect(() => {
        if(field_accordion.length < 2){
            setActive(0)
        }
    }, [field_accordion]);

    const activate = (index) => {
        setActive((index === active) ? null : index)
    }

    const accordionItem = ({first, second}, index) => {
        const className = (mod) => {
            return type + mod + (active === index ? ' active' : '')
        }
        return (
            <div key={index} className={className('__item')}>
                <div className={className('__item-heading')} onClick={() => activate(index)}>{first}</div>
                <div className={className('__item-content full_html')} dangerouslySetInnerHTML={{__html:second}}/>
            </div>
        )
    }
    return (
        <div className="container --containerFaq">
            <div className={type + '__wrapper'}>
                {(field_accordion || []).map(accordionItem)}
            </div>
        </div>)
}

AccordionComponent.propTypes = BlockPropTypes.AccordionPropTypes

export default AccordionComponent;
