import {GetBodyValue} from 'Util/LayoutBuilder';
import 'Styles/components/Basic.scss'
import BlockPropTypes from 'Types/block.proptypes';

const BasicComponent = ({data}) => {
    return (
        <div className='container'>
            <GetBodyValue value={data.attributes.body}/>
        </div>
    )
}

BasicComponent.propTypes = BlockPropTypes.BasicPropTypes

export default BasicComponent;
