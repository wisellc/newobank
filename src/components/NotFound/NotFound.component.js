import Head from 'next/head';
import 'Styles/components/NotFound.scss'

const NotFoundComponent = () => {


    return <>
        <Head>
            <title>O.Bank | {__('Not found')}</title>
        </Head>
        <div className="not_found">
            <img src="/sites/default/files/2020-11/not_found.png" alt=""/>
            <p>
                {__('The page you wanted to go to was not found')} 
            </p>
            <p>
                {__('Perhaps an incorrect address was entered or the page was deleted')}
            </p>
        </div>
    </>
}

export default NotFoundComponent