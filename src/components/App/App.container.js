import Header from 'Components/Header';
import configureStore from 'Store/index';
import {Provider} from 'react-redux';
import Footer from 'Components/Footer';
import App from './App';
import PropTypes from 'prop-types';
import LangDetector from '../../../config/SiteLang/LangDetector';
import Popup from 'Components/Popup';
import popupActivate from 'Util/Сrutches/popup.activate';

const AppContainer = ({asPath}) => {
    LangDetector.set(asPath);
    popupActivate()
    return (
        <Provider store={configureStore()}>
            <Header/>
            <App/>
            <Footer/>
            <Popup/>
        </Provider>
    )
}

AppContainer.propTypes = {
    asPath: PropTypes.string
}

export default AppContainer
