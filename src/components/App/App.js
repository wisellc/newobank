import {useDispatch, useSelector} from 'react-redux';
import Meta from 'Components/Meta';
import {renderLayoutBuilder} from 'Util/LayoutBuilder';
import NotFound from 'Components/NotFound';
import UserNode from 'Util/LayoutBuilder/userNode';
import Breadcrumbs from 'Components/Breadcrumbs';
import {activatePopup, disablePopup} from 'Store/Popup/popup.actions';
import {useEffect} from 'react';
import {isMobile} from 'Util/Сrutches/popup.activate';
const IMG_PATH = '/assets/img/svg/';

const App = () => {
    const {node} = useSelector(state => state.pageReducer.page)
    const dispatch = useDispatch();

    function renderPopUp() {
        return (
            <div className={'baner_v2_qr'}>
                <div className={'baner_v2_qr-close'} onClick={() => dispatch(disablePopup())}>
                    <img src={IMG_PATH + 'ic_cancel.svg'} alt={'close'}/>
                </div>
                <span className={'baner_v2_qr-title'}>Скануйте та відкривайте депозит у мобільному O.Bank 2.0</span>
                <img src={IMG_PATH + 'qr.png'} className={'baner_v2_qr-image'} alt={'qr code'}/>
            </div>
        )
    }

    useEffect(() => {
        const listener = (e) => {
            if(e.target.href === 'https://ideabank.page.link/xAwj') {
                if(!isMobile()){
                    e.preventDefault();
                    dispatch(activatePopup('qr', renderPopUp()))
                }
            }
        }

        window.addEventListener('click', listener);
        return () => {
            window.removeEventListener('click', listener)
        }
    }, []);

    if (!node) {
        return <NotFound/>
    }
    const {attributes, type} = node;

    if (type === 'user--user') {
        return (
            <>
                <Meta/>
                <main>
                    <Breadcrumbs/>
                    <UserNode node={node}/>
                    {renderLayoutBuilder(node)}
                </main>
            </>
        )
    }
    return (
        <>
            <Meta/>
            <main>
                <Breadcrumbs/>
                {attributes.field_show_title &&
                    <h1 className={'page-title'}>{attributes.title}</h1>}
                {renderLayoutBuilder(node)}
            </main>
        </>
    )
}

export default App;
