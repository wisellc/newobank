import {GetBodyValue} from 'Util/LayoutBuilder';
import {getParagraphType} from 'Paragraph/index';
import BlockPropTypes from 'Types/block.proptypes';
import dynamic from 'next/dynamic';
const FormaNaBanner = dynamic(() => import( 'Form/FormaNaBanner'), {ssr: false});

const PoluchitSsylkuComponent = ({data}) => {
    const {
        attributes: {body},
        relationships: {field_forma_na_banner},
        type: initialType
    } = data
    const type = getParagraphType(initialType);
    return (
        <div className='container'>
            <div className={type + '__wrapper'}>
                {field_forma_na_banner && <FormaNaBanner data={field_forma_na_banner}/>}
                <GetBodyValue value={body} className={type + '__text'}/>
            </div>
        </div>
    )
}

PoluchitSsylkuComponent.propTypes = BlockPropTypes.PoluchitSsylkuPropTypes

export default PoluchitSsylkuComponent
