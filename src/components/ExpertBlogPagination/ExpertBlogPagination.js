import 'Styles/components/_pagination.sass'
import {useRouter} from 'next/router';
import { number , shape } from 'prop-types';

const ExpertBlogPagination = (props) => {
    const {pager: {options: {items_per_page}, current_page, total_items}} = props;

    const router = useRouter();

    const {query, pathname} = router;
    const length = Math.ceil(total_items / items_per_page);

    const pages = Array.from({length}, (v, i) => i).map((_, index) => {
        return {
            value: index,
            page: index + 1
        }
    })

    const nextBtnDisabled = () => {
        return current_page >= length - 1
    }

    const goTo = (page) => {
        router.push({
            pathname,
            query: {
                ...query,
                page
            }
        })
    }


    if (length <= 1) {
        return null
    }

    return (
        <ul className={'pagination'} role={'navigation'}>
            <li>
                <div onClick={() => current_page ? goTo(current_page - 1) : null}
                     className={'pagination-item arrow prev ' + (current_page === 0 ? 'disabled' : '')}
                     role={'button'}/>
            </li>
            {pages.map(item =>
                <li key={item.value}>
                    <div
                        className={'pagination-item ' + (item.value === current_page ? 'active' : '')}
                        role={'button'}
                        onClick={() => goTo(item.value)}>{item.page}</div>
                </li>)}
            <li>
                <div onClick={() => !nextBtnDisabled() ? goTo(current_page + 1) : null}
                     className={'pagination-item arrow next ' + (nextBtnDisabled() ? 'disabled' : '')}
                     role={'button'}/>
            </li>
        </ul>
    )
}


ExpertBlogPagination.propTypes = {
    pager:shape({
        options: shape({items_per_page: number}),
        current_page: number,
        total_items: number
    })
}

export default ExpertBlogPagination
