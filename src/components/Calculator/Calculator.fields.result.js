import {any, bool, shape, string} from 'prop-types';
import Link from 'Components/Link';
import {numberTextFormat} from 'Components/Calculator/Calculator.config';

const CalculatorFieldsResult = ({calculatorResult = {}, field_link, loading}) => {
    const {currency, response: {type, message} = {}} = calculatorResult;
    const currencyString = ', ' + __(currency);

    const resultMap = {
        productName: {
            title: __('Name of the deposit'),
            value: calculatorResult.productName
        },
        termin: {
            title: __('Term month'),
            value: calculatorResult.termin
        },
        summa: {
            title: __('Amount') + currencyString,
            value: numberTextFormat(calculatorResult.summa)
        },
        depositRate: {
            title: __('Rate,%'),
            value: numberTextFormat(calculatorResult.depositRate) + '%'
        },
        oddo: {
            title: __('Income before taxation') + currencyString,
            value: numberTextFormat(calculatorResult.oddo)
        },

        taxes: {
            title: __('Taxes') + currencyString,
            value: numberTextFormat(calculatorResult.taxes)
        },

        rate: {
            title: __('Rate including taxes,%'),
            value: numberTextFormat(calculatorResult.rate) + '%'
        },

        yourMoney: {
            title: __('Your income') + currencyString,
            value: numberTextFormat(calculatorResult.yourMoney)
        },

        rates: {
            title: __('Payment for additional and related services') + currencyString,
            value: numberTextFormat('0,00')
        },

    }

    const renderResultItem = ([key, item]) => (
        <div key={key} className={'calculator__result-item ' + key}>
            <span>{item.title}</span>
            <p>{item.value}</p>
        </div>
    )

    const renderContent = () => {
        if (type === 'FAILURE') {
            return <span>{message}</span>
        }
        return (
            <>
                <span className="calculator__title">{__('Best Deal')}</span>
                {Object.entries(resultMap).map(renderResultItem)}
                <div className="podatok">
                    {__('* personal income tax 18% and military tax 1.5%')}
                </div>
                <Link {...field_link} url={field_link.uri} className='button '/>
            </>
        )
    }

    return <div className={'calculator__right' + (loading ? ' loading' : '')}>
        {renderContent()}
    </div>
}


CalculatorFieldsResult.propTypes = {
    calculatorResult: any,
    loading: bool,
    field_link: shape({
        options: any,
        uri: string,
        title: string
    })
}

export default CalculatorFieldsResult
