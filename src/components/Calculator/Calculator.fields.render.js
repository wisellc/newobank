import Select from 'react-select';
import {any, arrayOf, func, number, oneOfType, shape, string} from 'prop-types';
import 'Styles/components/Calculator.scss'

const CalculatorFieldsRender = ({fields, formResult, setFormResult, field_block_title}) => {

    const changeForm = (name, value) => {
        const result = {...formResult}
        result[name.toString()] = value;
        setFormResult(JSON.parse(JSON.stringify(result)))
    }

    const select = (res) => {
        changeForm(res.question, res.value)
    }

    const customFieldsValue = ({target}) => {
        if (parseInt(target.value) <= parseInt(target.max) &&
            parseInt(target.min) <= parseInt(target.value)
        ) {
            changeForm(target.name, target.value)
        }
    }

    const renderSelect = (data, defaultIndex) => {
        return <Select onChange={select}
                       className={'select-wrapper'}
                       classNamePrefix={'select'}
                       defaultValue={data[defaultIndex]}
                       options={data}/>
    }

    const renderButton = (data) => {
        const getActive = (value) => {
            // eslint-disable-next-line no-prototype-builtins
            if (formResult.hasOwnProperty(value?.question?.toString())) {
                const current = formResult[value.question.toString()];
                return current && (current === value.value);
            }
            return false
        };
        return <div className={'cur-switcher'}>
            {data && data.map((currency, index) =>
                <div onClick={() => select(currency)}
                     key={currency.value}
                     className={'cur-switcher__btn' + (getActive(currency) ? ' active' : '')}
                >{currency.label}</div>
            )}
        </div>
    }

    const renderCheckbox = (data, defaultIndex, label) => {
        const check = (e) => {
            if (e.target.checked) {
                select(data.find(item => item.controlType === 1))
            } else {
                select(data.find(item => item.controlType === 0))
            }
        };

        const getStatus = () => {
            return data.find(item => item.controlType === 1)?.default
        };

        return (
            <div className={'form_field checkbox'}>
                <input
                    type={'checkbox'}
                    name={label}
                    id={defaultIndex + label}
                    defaultChecked={getStatus()}
                    onChange={event => check(event)}
                />
                <label htmlFor={defaultIndex + label}/>
                <label htmlFor={defaultIndex + label} className={'label_text full_html'}>{label}</label>
            </div>
        )
    }

    const getFormByType = (item) => {
        return <div className={'form_field range'}>
            <input type={'number'} max={item.max}
                   onChange={(e) => customFieldsValue(e)}
                   name={item.key}
                   min={item.min} value={formResult[item.key] || item.default}/>
            <div className={'range-line-wrapper'}>
                <div className={'range-line'}
                     style={{
                         width: ((
                             ((formResult[item.key] || item.default) - item.min) / (item.max - item.min)) * 100) + '%'
                     }}/>
            </div>
            <input type={item.type} max={item.max} min={item.min} name={item.key}
                   value={formResult[item.key] || item.default}
                   onChange={(e) => customFieldsValue(e)}/>
        </div>
    }

    const getFields = (field) => {
        const {id, controlType, text, values} = field;
        if (typeof controlType === 'number') {
            if (values) {
                const data = [];
                let defaultIndex = 0;
                values.forEach(item => data.push({
                    value: item.id,
                    label: item.text,
                    question: id,
                    default: item.default,
                    controlType: item.controlType
                }));
                data.forEach((item, index) => item.default ? defaultIndex = index : null);
                const inputFields = [
                    renderSelect(data, defaultIndex),
                    renderButton(data, defaultIndex),
                    renderCheckbox(data, defaultIndex, text)
                ];
                try {
                    return inputFields[controlType]
                } catch (e) {
                    return inputFields[0]
                }
            }
        } else {
            return getFormByType(field)
        }
        return false
    }

    const form = () => {
        return fields.map(item =>
            <div key={item.id}>
                {item.controlType !== 2 &&
                <label>{item.text}</label>}
                {getFields(item)}
            </div>
        )
    }

    return <>
        <div className="calculator__left">
            <span className="calculator__title">{field_block_title}</span>
            {form()}
        </div>
    </>
}

CalculatorFieldsRender.propTypes = {
    fields: arrayOf(shape({
        id: oneOfType([string, number]),
        text: string
    })),
    field_block_title: string,
    formResult: any,
    setFormResult: func
}

export default CalculatorFieldsRender
