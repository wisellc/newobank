import {calc, getAnswer, sendCalculator} from 'Components/Calculator/Calculator.config';
import {any, arrayOf, number, oneOfType, shape, string} from 'prop-types';
import {useEffect, useState} from 'react';
import CalculatorFieldsRender from 'Components/Calculator/Calculator.fields.render';
import CalculatorFieldsResult from 'Components/Calculator/Calculator.fields.result';

const CalculatorFields = (props) => {
    const {field_calculator_type, custom_fields, field_link, field_block_title} = props;
    const [fields, setFields] = useState(custom_fields);
    const [calculatorResult, setCalculatorResult] = useState({});
    const [formResult, setFormResult] = useState({})
    const [loading, setLoading] = useState(false);

    const processArray = (array) => {
        const promises = array.map(({id}) => getAnswer('GetAnswers', id));
        return Promise.all(promises);
    }

    useEffect(() => {
        const customData = {};
        const arr = {};
        Object.entries(formResult).forEach(([key, value]) => {
            if (isNaN(parseInt(key, 10))) {
                customData[key] = value
            } else {
                arr[key] = value
            }
        })
        const keys = Object.keys(arr);
        if (keys.length === 0) return null;
        let res = '';
        keys.forEach((item, index) => {
            res = res + arr[item] + (index + 1 === keys.length ? '' : ',')
        });
        setLoading(true)
        calc(field_calculator_type, res, customData).then(a => {
            setCalculatorResult(a)
            setLoading(false)
        })
    }, [field_calculator_type, formResult])

    useEffect(() => {
        sendCalculator('GetQuestions', field_calculator_type).then(({questions}) => {
            processArray(questions).then(a => {
                const apiFields = questions;
                const apiValue = {};
                a.forEach(({answers}, index) => {
                    apiFields[index].values = answers
                    apiValue[apiFields[index].id.toString()] = answers.find(item => item.default)?.id
                })
                custom_fields.forEach(b => {
                    apiValue[b.id] = b.default
                    apiFields.push(b)
                })
                const fields = apiFields.sort((a, b) =>
                    (a.order && b.order) ? a.order < b.order ? -1 : 1 : 1
                )
                setFields(fields);
                setFormResult(apiValue)
            })
        })
    }, [custom_fields, field_calculator_type])

    const calculatorFieldsRenderProps = {
        fields,
        formResult,
        setFormResult,
        field_block_title
    }

    const calculatorFieldsResultProps = {
        calculatorResult,
        loading,
        field_link
    }

    return <div className={'calculator__wrapper'}>
        <CalculatorFieldsRender
            {...calculatorFieldsRenderProps}
        />
        <CalculatorFieldsResult
            {...calculatorFieldsResultProps}
        />
    </div>
}
CalculatorFields.propTypes = {
    field_calculator_type: string,
    field_block_title: string,
    field_link: shape({
        options: any,
        uri: string,
        title: string
    }),
    custom_fields: arrayOf(shape({
        default: oneOfType([string, number]),
        id: string,
        order: number
    }))
}

export default CalculatorFields;
