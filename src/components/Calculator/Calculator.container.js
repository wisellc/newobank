import CalculatorComponent from './Calculator.component';
import {any, arrayOf, shape, string} from 'prop-types';

const CalculatorContainer = ({data}) => {
    const {
        attributes: {field_calculator_type, field_link, field_block_title},
        relationships: {field_custom_data}
    } = data;

    const getCustomFields = () => {
        const result = [];
        field_custom_data.data.forEach(({data: {attributes}}) => {
            attributes.order = attributes.field_order
            result.push({
                type: attributes.field_type,
                max: attributes.field_max,
                min: attributes.field_min,
                id: attributes.field_key,
                default: attributes.field_default,
                key: attributes.field_key,
                order: attributes.field_order,
                text: attributes.field_title
            })
        })
        return result
    }

    const componentProps = {
        custom_fields: getCustomFields(),
        field_calculator_type,
        field_link,
        field_block_title
    }

    return <CalculatorComponent
        {...componentProps}
    />
}

CalculatorContainer.propTypes = {
    data: shape({
        attributes: shape({
            field_calculator_type: string,
            field_block_title: string
        }),
        relationships: shape({
            field_custom_data: shape({
                data: arrayOf(shape({
                    data: shape({
                        attributes: any
                    })
                }))
            })
        })
    })
}

export default CalculatorContainer
