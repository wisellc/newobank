import {sendPostByUrl} from 'Util/Request';

export const getTypeByCalculatorType = (type) => {
    const result = {
        type: '',
        typePerson: type.indexOf('Ind') !== -1 ? 0 : 1
    };
    if (type.indexOf('CreditCalc') !== -1) {
        result.type = 1
    } else if (type.indexOf('CreditCardCalc') !== -1) {
        result.type = 3
    } else if (type.indexOf('DepositCalc') !== -1) {
        result.type = 2
    } else if (type.indexOf('PlatijCardCalc') !== -1) {
        result.type = 5
    } else if (type.indexOf('OverDraft') !== -1) {
        result.type = 4
    } else if (type.indexOf('AcountsCalc') !== -1) {
        result.type = 6
    }
    return result
}

const langList = {
    uk: 1,
    ru: 2,
    en: 3
};

export const calculatorLang = () => langList[getLang()];

export const sendCalculator = (method, type) => {
    const body = Object.assign({
        language: calculatorLang()
    }, getTypeByCalculatorType(type));
    const apiUrl = process.env.REACT_APP_API_URL + '/' + getLang() + '/jsonapi/wise/proxyrequest?method=';
    const url = apiUrl + method;
    return sendPostByUrl(url, JSON.stringify(body))
}

export const getAnswer = (method, id) => {
    const body = {
        language: calculatorLang()
    };
    body.typeAnswer = id;
    const apiUrl = process.env.REACT_APP_API_URL + '/' + getLang() + '/jsonapi/wise/proxyrequest?method=';
    const url = apiUrl + method;
    return sendPostByUrl(url, JSON.stringify(body))
}

export const calc = (method, answer, additional) =>  {
    const body = Object.assign({
        language: calculatorLang(),
        answerId: answer
    }, additional);
    const apiUrl = process.env.REACT_APP_API_URL + '/' + getLang() + '/jsonapi/wise/proxyrequest?method=';
    const url = apiUrl + method;
    return sendPostByUrl(url, JSON.stringify(body))
}

export const numberTextFormat = (data) => {
    if (typeof data !== 'number') {
        return data;
    } else {
        return data
            .toString()
            .replace('.', ',')
            // eslint-disable-next-line
            .replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ');
    }
}
