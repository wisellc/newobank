import CalculatorFields from './Calculator.fields';
import {any, shape, string} from 'prop-types';

const CalculatorComponent = (props) => {
    const {custom_fields, field_calculator_type, field_link, field_block_title} = props;

    return (
        <CalculatorFields
            field_block_title={field_block_title}
            field_calculator_type={field_calculator_type}
            custom_fields={custom_fields}
            field_link={field_link}
        />
    )
}

CalculatorComponent.propTypes = {
    field_calculator_type: string,
    field_block_title: string,
    field_link: shape({
        options: any,
        uri: string,
        title: string
    }),
    custom_fields: any,
    numberTextFormat: any
}

export default CalculatorComponent
