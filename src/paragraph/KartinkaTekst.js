import Image from 'Components/Image';
import {GetBodyValue} from 'Util/LayoutBuilder';
import {getParagraphType, ParagraphTitle, resolveParagraphItems} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const KartinkaTekst = ({type: wrapperType, attributes: {field_text_paragraph, field_title}, relationships: {field_image_paragraph, field_paragraph}}) => {
    const type = getParagraphType(wrapperType);
    return (
        <div className={type + '__wrapper'}>
            <div className={`${type}__image`}>
                <Image data={field_image_paragraph.data} webp={field_image_paragraph.data?.webp} className={type + '__images'}/>
            </div>
            <div className={type + '__description'}>
                <ParagraphTitle type={type} field_title={field_title}/>
                <GetBodyValue value={field_text_paragraph} className={type + '__text'}/>
                {field_paragraph.data.map(resolveParagraphItems)}
            </div>
        </div>
    )
}

KartinkaTekst.propTypes = ParagraphPropTypes.TekstKartinkaPropTypes

export default KartinkaTekst;
