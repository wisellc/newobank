import Image from 'Components/Image';
import {GetBodyValue} from 'Util/LayoutBuilder';
import {getParagraphType, ParagraphTitle} from 'Paragraph/index';
import 'Styles/paragraph/InfoBlokPopup';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const InfoBlokPopup = (props) => {
    const {attributes: {field_text_paragraph, field_title}, relationships, index, type: initialType} = props;
    const type = getParagraphType(initialType)
    return (
        <div
            className={type + '__item '}
            key={index}
        >
            <div className={`${type}__image`}>
                <Image data={relationships.field_icon.data.relationships.field_ikon.data}/>
            </div>
            <ParagraphTitle type={type} field_title={field_title}/>
            <GetBodyValue
                className={type + '__description'}
                value={field_text_paragraph}
            />
        </div>
    )
}

InfoBlokPopup.propTypes = ParagraphPropTypes.InfoBlokPopupPropTypes

export default InfoBlokPopup
