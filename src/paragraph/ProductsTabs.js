import BlockSorting from 'Util/BlockSorting';
import {getParagraphType} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const ProductsTabs = ({data, type: wrapperType}) => {
    const type = getParagraphType(wrapperType);
    return (
        <div className={type + '__wrapper'}>
            {data.map(item => <BlockSorting
                key={item.id}
                data={item}
                config={{type: getParagraphType(item.type)}}
            />)}
        </div>
    )
}

ProductsTabs.propTypes = ParagraphPropTypes.ProductsTabsPropTypes

export default ProductsTabs
