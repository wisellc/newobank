import {GetBodyValue} from 'Util/LayoutBuilder';
import {getParagraphType, ParagraphTitle} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const TextArea = ({attributes, type: initType}) => {
    const type = getParagraphType(initType);
    return <div className={type + '__wrapper'}>
        <ParagraphTitle type={type} field_title={attributes.field_title}/>
        <GetBodyValue value={attributes.field_text_paragraph} className={type + '__text'}/>
    </div>
}

TextArea.propTypes = ParagraphPropTypes.TextAreaPropTypes;

export default TextArea
