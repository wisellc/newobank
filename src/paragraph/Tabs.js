import {resolveParagraphItems} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const Tabs = ({relationships: {field_tabs}, active}) => {
    return <div className={'tab__wrapper' + (active ? ' active' : '')}>
        {field_tabs.data.map(resolveParagraphItems)}
    </div>
}

Tabs.propTypes = ParagraphPropTypes.TabsPropTypes

export default Tabs
