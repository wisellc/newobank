import Image from 'Components/Image';
import {getParagraphType, ParagraphTitle} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const LogoLending = ({type: wrapperType, attributes: {field_title}, relationships: {field_image_paragraph}}) => {
    const type = getParagraphType(wrapperType);
    return (
        <div className={type + '__wrapper'}>
            <ParagraphTitle type={type} field_title={field_title}/>
            <div className={`${type}__image`}>
                <Image data={field_image_paragraph.data} className={type + '__images'}/>
            </div>
        </div>
    )
}
LogoLending.defaultProps = {
    attributes: {},
    relationships: {
        field_image_paragraph: {}
    },
    type: 'paragraph--logo_lending'
}

LogoLending.propTypes = ParagraphPropTypes.LogoLendingPropTypes;

export default LogoLending
