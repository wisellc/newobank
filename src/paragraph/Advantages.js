import {getParagraphType, ParagraphTitle} from 'Paragraph/index';
import ParagraphPropTypes from 'Types/paragraph.proptypes';

const Advantages = ({type: initialType, attributes: {field_title, field_subtitle}}) => {
    const type = getParagraphType(initialType);
    return (
        <div className={type + '__wrapper'}>
            <ParagraphTitle type={type} field_title={field_title}/>
            {field_subtitle && <span className={type + '__subtitle'}>{field_subtitle}</span>}
        </div>
    )
}
Advantages.defaultProps = {
    type: 'paragraph--advantages',
    attributes: {
        field_title: ''
    }
}
Advantages.propTypes = ParagraphPropTypes.AdvantagesPropTypes;

export default Advantages;
