import KartinkaTekst from 'Paragraph/KartinkaTekst';
import TekstKartinka from 'Paragraph/TekstKartinka';
import TextArea from 'Paragraph/TextArea';
import InfoBlokPopup from 'Paragraph/InfoBlokPopup';
import Advantages from 'Paragraph/Advantages';
import LogoLending from 'Paragraph/LogoLending';
import File from 'Paragraph/File';
import PropTypes from 'prop-types';
import Tabs from 'Paragraph/Tabs';
import ProductsTabs from 'Paragraph/ProductsTabs';

const ParagraphList = {
    'kartinka_tekst': KartinkaTekst,
    'tekst_kartinka': TekstKartinka,
    'text_area': TextArea,
    'info_blok_popup': InfoBlokPopup,
    'advantages': Advantages,
    'logo_lending': LogoLending,
    'file': File,
    'tabs': Tabs,
    'products_tabs': ProductsTabs
};

export const resolveParagraphItems = (props, index) => {
    if(!props) return null;
    const {type, data, attributes} = props;
    const componentType = getParagraphType(type)
    const Paragraph = ParagraphList[componentType]
    const childProps = attributes ? props : data
    if (Paragraph) {
        return <Paragraph {...childProps} key={index}/>
    }
    console.log(componentType);
}

export const ParagraphTitle = ({field_title, type}) => {
    if (!field_title) return null
    return (
        <span className={'paragraph-title ' + type + '__title'}>
            {field_title}
        </span>
    )
}
ParagraphTitle.propTypes = {
    field_title: PropTypes.any,
    type: PropTypes.string
}

export const getParagraphType = (type) => type.split('--')[1]

export default ParagraphList
