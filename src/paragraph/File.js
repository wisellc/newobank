import {getParagraphType} from 'Paragraph/index';
import RenderFile from 'Components/File';
import {FileParagraphPropTypes} from 'Types/file.propTypes';

const File = ({type: wrapperType, relationships: {field_file, included}, data}) => {
    const type = getParagraphType(wrapperType);

    const getData = (item) => {
        if (item.data) return item.data;
        if (included) return included.find(a => a.id === item.id)
        if (data) return data.find(a => a.id === item.id)
        return null
    }

    return (
        <div className={type + '__wrapper'}>
            {field_file.data.map((item, index) => <RenderFile key={index} {...item} data={getData(item)}/>)}
        </div>
    )

}

File.propTypes = FileParagraphPropTypes

export default File
