import Link from 'Components/Link';
import 'Styles/components/_experts.sass';
import SocialNetworks from 'Components/SocialNetworks/SocialNetWorks';
import PropTypes, {arrayOf, oneOfType, shape} from 'prop-types';
import Image from 'next/image';

const ExpertsComponent = (props) => {
    const {data, additional: {css_classes}} = props;

    const isList = !data.data;

    const renderExpert = (
        {
            user_picture,
            field_dosvid_roboty,
            field_sotsialni_merezhi,
            field_user_name,
            field_user_surname,
            view_user
        }, index
    ) => (
        <div key={index} className="expert">
            <div className="expert__img">
                <Image src={isList ? user_picture : user_picture.url} layout="fill" alt={field_user_surname}/>
            </div>
            <h3>{field_user_name + ' ' + field_user_surname}</h3>
            <span>{field_dosvid_roboty}</span>
            <SocialNetworks field_sotsialni_merezhi={field_sotsialni_merezhi}/>
            <Link
                url={view_user}
                className="expert__link"
            >
                {__('Read more')}
            </Link>
        </div>
    )

    return (
        <section className={'experts ' + css_classes}>
            <div className="container">
                <div className="experts__wrap">
                    {(isList ? data : data.data).map(renderExpert)}
                </div>
            </div>
        </section>
    );
}

ExpertsComponent.propTypes = {
    data: oneOfType([
        arrayOf(shape({
            user_picture: PropTypes.string,
            field_dosvid_roboty: PropTypes.string,
            field_sotsialni_merezhi: PropTypes.arrayOf(PropTypes.shape({
                url: PropTypes.string
            })),
            field_user_name: PropTypes.string,
            field_user_surname: PropTypes.string,
            view_user: PropTypes.string
        })),
        shape({
            data: arrayOf(shape({
                user_picture: PropTypes.shape({
                    url: PropTypes.string
                }),
                field_dosvid_roboty: PropTypes.string,
                field_user_name: PropTypes.string,
                field_user_surname: PropTypes.string,
                view_user: PropTypes.string
            }))
        })
    ]),
    additional: PropTypes.shape({
        css_classes: PropTypes.string
    })
}

export default ExpertsComponent
