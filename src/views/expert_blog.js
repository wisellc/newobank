import 'Styles/components/_expert_blog.sass';
// import { Menu } from '../blocks/menu/menu';
import ExpertBlogPagination from 'Components/ExpertBlogPagination/ExpertBlogPagination';
import ExpertBlogFilter from 'Components/ExpertBlogFilter/ExpertBlogFilter';
import Link from 'Components/Link';
import PropTypes from 'prop-types';
import Image from 'next/image';


const ExpertBlog = (props => {
    const {
        data: {'category-list': categoryList, pager, data = []},
        configuration: {views_label, label_display},
        additional: {css_classes}
    } = props;

    const renderPost = ({
                            body, field_blog_image, title, field_data,
                            field_user_surname,
                            field_user_name,
                            user_picture,
                            view_node
                        }, index) => (

        <article key={index} className="expert-article">
            <div className="expert-article__img">
                <Image src={field_blog_image.url} {...field_blog_image.meta}/>
            </div>
            <div className="expert-article__info">
                <Image src={user_picture.url} {...user_picture.meta}/>
                <span>{field_user_name + ' ' + field_user_surname}</span>
            </div>
            <Link url={view_node} className="expert-article__link">
                {title}
            </Link>
            <time>{field_data}</time>
            <div className="expert-article__text" dangerouslySetInnerHTML={{__html: body}}/>
        </article>
    );

    const renderNoItems = () => {
        if(data.length){
            return null
        }

        return (
            <div className='expert-blog__empty'>{__('Sorry, articles not found!')}</div>
        )
    }

    return (
        <section className={'expert-blog ' + css_classes}>
            <div className="container">
                <ExpertBlogFilter
                    categoryList={categoryList}/>
                {(label_display === 'visible') && <h6 className="expert-blog__title">{views_label}</h6>}
                <div className="expert-blog__wrap">
                    {data.map(renderPost)}
                    {renderNoItems()}
                </div>
                <ExpertBlogPagination
                    pager={pager}
                />
                <div className="expert-blog__social-share">
                    {/* <h6>{__('Share this page')}</h6> */}
                    {/*<Menu children={'social-share'} />*/}
                </div>
            </div>
        </section>
    )
});


ExpertBlog.propTypes = {
    data: PropTypes.shape({
        'category-list': PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string
        })),
        pager: PropTypes.shape({
            options: PropTypes.shape({items_per_page: PropTypes.number}),
            current_page: PropTypes.number,
            total_items: PropTypes.number
        }),
        data: PropTypes.arrayOf(PropTypes.shape({
            body: PropTypes.string,
            field_blog_image: PropTypes.shape({
                url: PropTypes.string,
                meta: PropTypes.shape
            }),
            title: PropTypes.string,
            field_data: PropTypes.string,
            field_user_surname: PropTypes.string,
            field_user_name: PropTypes.string,
            user_picture: PropTypes.shape({
                url: PropTypes.string,
                meta: PropTypes.shape
            }),
            view_node: PropTypes.string
        }))
    }),
    configuration: PropTypes.shape({
        views_label: PropTypes.string,
        label_display: PropTypes.string
    }),
    additional: PropTypes.shape({
        css_classes: PropTypes.string
    })
}


export default ExpertBlog
