import Experts from './Experts';
import ExpertBlog from './expert_blog';
import {shape, string} from 'prop-types';


const Views = (props) => {
    const {additional: {block_id}} = props;

    const viewsList = {
        experts: Experts,
        expert_blog: ExpertBlog
    }

    const View = viewsList[block_id]

    if (View) {
        return <View {...props} />
    }

    console.log(block_id)

    return null;
}

Views.propTypes = {additional: shape({block_id: string})}

export default Views
